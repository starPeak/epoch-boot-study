package com.epoch.sys;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import springfox.documentation.oas.annotations.EnableOpenApi;
/**
 * 
 * @author zhangqinglei
 * @date 2020/12/02
 * @qq 544188838
 */	
@EnableAutoConfiguration
@Configuration
@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages="com.epoch")
@MapperScan("com.epoch.sys.dao")
@EnableOpenApi
@EnableAspectJAutoProxy
@EnableZuulProxy
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
