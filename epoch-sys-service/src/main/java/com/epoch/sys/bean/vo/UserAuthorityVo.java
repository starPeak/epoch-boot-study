package com.epoch.sys.bean.vo;

import java.io.Serializable;
import java.util.List;

import com.epoch.sys.common.bean.TreeNode;

/**
 * 用户权限返回对象
 * 包含有
 * @author zhangqinglei
 * @date 2021/01/09
 * @qq 544188838
 */
public class UserAuthorityVo implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 返回当前权限菜单
	 */
	public List<TreeNode> menuList;
	
	/**
	 * 拥有的权限码列表
	 */
	public List<Integer> authorityCodes;
	
	public SysInfoVo sysInfo;
	
	public SysUserVo currentUser;

	public List<TreeNode> getMenuList() {
		return menuList;
	}

	public void setMenuList(List<TreeNode> menuList) {
		this.menuList = menuList;
	}

	public List<Integer> getAuthorityCodes() {
		return authorityCodes;
	}

	public void setAuthorityCodes(List<Integer> authorityCodes) {
		this.authorityCodes = authorityCodes;
	}

	public SysInfoVo getSysInfo() {
		return sysInfo;
	}

	public void setSysInfo(SysInfoVo sysInfo) {
		this.sysInfo = sysInfo;
	}

	public SysUserVo getCurrentUser() {
		return currentUser;
	}

	public void setCurrentUser(SysUserVo currentUser) {
		this.currentUser = currentUser;
	}
}
