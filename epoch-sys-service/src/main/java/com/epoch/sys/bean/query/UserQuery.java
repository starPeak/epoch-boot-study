package com.epoch.sys.bean.query;

/**
 * 用户对象查询类
 * @author zhangqinglei
 * @date 2021/01/07
 * @qq 544188838
 */
public class UserQuery extends BaseQuery {
	
	private String account;
	
	private String realName;
	
	private String phone;
	
	private String email;
	
	private Integer status;
	
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
}
