package com.epoch.sys.bean.vo;

import java.util.List;

import com.epoch.common.bean.BaseTreeNode;

public class DictCategoryTree extends BaseTreeNode{

	private int count;
	
	private List<DictCategoryTree> children;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<DictCategoryTree> getChildren() {
		return children;
	}

	public void setChildren(List<DictCategoryTree> children) {
		this.children = children;
	}
	
}
