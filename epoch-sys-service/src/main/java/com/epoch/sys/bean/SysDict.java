package com.epoch.sys.bean;

import java.io.Serializable;
import java.util.Date;

public class SysDict implements Serializable {
    private Integer id;

    private String dictCode;

    private Integer dictCategoryId;

    private String dictName;

    private Integer dictFlag;

    private Date createDate;

    private Date updateDate;

    private String dictComments;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDictCode() {
        return dictCode;
    }

    public void setDictCode(String dictCode) {
        this.dictCode = dictCode;
    }

    public Integer getDictCategoryId() {
        return dictCategoryId;
    }

    public void setDictCategoryId(Integer dictCategoryId) {
        this.dictCategoryId = dictCategoryId;
    }

    public String getDictName() {
        return dictName;
    }

    public void setDictName(String dictName) {
        this.dictName = dictName;
    }

    public Integer getDictFlag() {
        return dictFlag;
    }

    public void setDictFlag(Integer dictFlag) {
        this.dictFlag = dictFlag;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getDictComments() {
        return dictComments;
    }

    public void setDictComments(String dictComments) {
        this.dictComments = dictComments;
    }
}