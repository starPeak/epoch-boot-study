 package com.epoch.sys.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;

import com.epoch.common.bean.ComFieldBean;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhangqinglei
 * @date 2020/12/20
 * @qq 544188838
 */
@ApiModel(value="账号对象",description="账号对象")
public class SysUserBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(required=false)
    private Integer id;

    @ApiModelProperty(value="用户名",example="epoch",required=true)
    @NotBlank(message="角色名称不能为空")
    private String account;

    @ApiModelProperty(value="密码",example="123456",required=true)
    @NotBlank(message="密码不能为空")
    private String password;

    @ApiModelProperty(value="真实姓名",example="汪峰",required=true)
    private String realName;

    @ApiModelProperty(value="邮箱",example="544188838@qq.com",required=false)
    private String email;

    @ApiModelProperty(value="电话",example="123456",required=false)
    private String phone;

    @ApiModelProperty(value="状态",required=false)
    private Integer status = 1;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createDate;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date updateDate;
    
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date loginDate;
    
    private String loginIp;
    
    private List<Integer> roleList;

    public Date getLoginDate() {
		return loginDate;
	}

	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

	public List<Integer> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Integer> roleList) {
		this.roleList = roleList;
	}
}
