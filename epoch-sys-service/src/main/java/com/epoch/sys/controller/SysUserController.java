package com.epoch.sys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.sys.bean.SysUserBean;
import com.epoch.sys.bean.query.UserQuery;
import com.epoch.sys.bean.vo.UserAuthorityVo;
import com.epoch.sys.common.util.SysConstant;
import com.epoch.sys.service.ISysUserService;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin
@Api(value = "用户相关操作controller", tags = {"用户权限操作接口"})
@RequestMapping("/epoch/sys/user")
public class SysUserController {
	
	@Autowired
	ISysUserService sysUserService;

	@PostMapping(path = "/selectUserList")
	public PageInfo<SysUserBean> selectUserList(UserQuery query) {
		return sysUserService.selectUserList(query);
	}
	
	@GetMapping(path = "/getUserAuthority")
	public UserAuthorityVo getUserAuthority(@RequestHeader Integer userId) {
		return sysUserService.getUserAuthority(userId);
	}
	
	@GetMapping(path = "/getUserInfo")
	public SysUserBean getUserInfo(@RequestParam Integer id) {
		return sysUserService.getUserInfo(id);
	}
	
	@PostMapping(path = "/addNewUser")
	public boolean addNewUser(@RequestBody SysUserBean userBean) {
		sysUserService.addNewUser(userBean);
		return true;
	}
	
	@DeleteMapping(path = "/delete")
	public boolean delete(@RequestBody List<Integer> roleIds) {
		sysUserService.deleteByIds(roleIds);
		return true;
	}
	
	@PostMapping(path = "/lock")
	public boolean lock(@RequestBody List<Integer> userIds) {
		sysUserService.updateStatus(userIds,SysConstant.LOCK_STATUS);
		return true;
	}
	
	@PostMapping(path = "/resetPassword")
	public boolean resetPassword(@RequestBody List<Integer> userIds) {
		sysUserService.resetPassword(userIds);
		return true;
	}
	
	@PostMapping(path = "/unlock")
	public boolean unlock(@RequestBody List<Integer> userIds) {
		sysUserService.updateStatus(userIds,SysConstant.UN_LOCK_STATUS);
		return true;
	}
	
	@PostMapping(path = "/update")
	public boolean update(@RequestBody SysUserBean userBean) {
		sysUserService.updateUser(userBean);
		return true;
	}
}
