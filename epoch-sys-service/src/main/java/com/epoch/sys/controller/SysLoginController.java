package com.epoch.sys.controller;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.sys.common.bean.SysLoginDto;
import com.epoch.sys.common.bean.SysUserDataDto;
import com.epoch.sys.service.ISysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * 单点登录相关操作
 * @author zhangqinglei
 * @date 2020/12/18
 * @qq 544188838
 */
@RestController
@CrossOrigin
@Api(value="登录权限相关操作controller",tags={"登录权限操作接口"})
@RequestMapping("/epoch/auth")
public class SysLoginController {

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 密码加密方法：epochV1_时间戳_加密钥匙_密码
     * 
     * @param params
     * @return
     */
    @ApiOperation(value = "用户登录", notes = "字段有account和password")
    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public SysUserDataDto login(@RequestBody SysLoginDto sysLoginDto) {
        return sysUserService.login(sysLoginDto);
    }

    /**
     * 
     * @param params
     * @param httpServletResponse
     * @return
     */
    @ApiOperation(value = "用户退出", notes = "")
    @ApiImplicitParams({@ApiImplicitParam(dataType = "String", name = "userId", required = true)})
    @RequestMapping(path = "/logout ", method = RequestMethod.POST)
    public boolean logout(@ApiParam @RequestParam(name = "userId") String userId,
        HttpServletResponse httpServletResponse) {
        sysUserService.logout();
        return true;
    }
}
