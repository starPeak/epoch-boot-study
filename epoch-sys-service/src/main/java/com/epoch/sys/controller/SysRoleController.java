package com.epoch.sys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.common.bean.ComFieldBean;
import com.epoch.sys.bean.SysRole;
import com.epoch.sys.bean.query.RoleQuery;
import com.epoch.sys.service.ISysRoleService;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin
@Api(value = "角色相关操作controller", tags = {"角色权限操作接口"})
@RequestMapping("/epoch/sys/role")
public class SysRoleController {
	
	@Autowired
	ISysRoleService sysRoleService;

	@PostMapping(path = "/selectRoleList")
	public PageInfo<SysRole> selectUserList(RoleQuery query) {
		return sysRoleService.selectRoleList(query);
	}
	
	@GetMapping(path = "/selectRoleInfoById")
	public SysRole selectRoleInfoById(Integer id) {
		return sysRoleService.selectByPrimaryKey(id);
	}
	
	@PostMapping(path = "/addRole")
	public int addRole(@RequestBody SysRole sysRole) {
		return sysRoleService.addRole(sysRole);
	}
	
	@DeleteMapping(path = "/delete")
	public boolean delete(@RequestBody List<Integer> roleIds) {
		sysRoleService.deleteByIds(roleIds);
		return true;
	}
	
	@PostMapping(path = "/update")
	public boolean update(@RequestBody SysRole userBean) {
		sysRoleService.updateRole(userBean);
		return true;
	}
	
	@GetMapping(path = "/getRoleComList")
	public List<ComFieldBean> getRoleComList(){
		return sysRoleService.getRoleComList();
	}
}
