package com.epoch.sys.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.epoch.common.bean.EpochTree;
import com.epoch.sys.bean.SysDict;
import com.epoch.sys.bean.SysDictCategory;
import com.epoch.sys.bean.query.DictQuery;
import com.epoch.sys.service.ISysDistService;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;

@RestController
@CrossOrigin
@Api(value = "数据字典相关操作controller", tags = {"数据字典操作接口"})
@RequestMapping("/epoch/sys/dict")
public class SysDictController {

	@Autowired
	ISysDistService sysDistService;
	
	@PostMapping(path = "/selectDictList")
	public PageInfo<SysDict>  selectDictList(DictQuery query){
		return sysDistService.selectDictList(query);
	}
	
	@PostMapping(path = "/selectDictCategoryList")
	public PageInfo<SysDictCategory>  selectDictCategoryList(DictQuery query){
		return sysDistService.selectSysDictCategoryList(query);
	}
	
	@GetMapping(path = "/selectDictCategoryTree")
	public EpochTree  selectDictCategoryTree(){
		return sysDistService.selectDictCategoryTree();
	}
	
	@GetMapping(path = "/selectDictCategoryTree2")
	public JSONObject  selectDictCategoryTree2(){
		JSONObject json = new JSONObject();
		json.put("expanded", true);
		List<EpochTree> tree = new ArrayList<EpochTree>();
		tree.add(sysDistService.selectDictCategoryTree());
		json.put("children", tree);
		return json;
	}
	
}
