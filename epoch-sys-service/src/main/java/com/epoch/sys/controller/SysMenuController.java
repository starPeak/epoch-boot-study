package com.epoch.sys.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.sys.common.bean.TreeNode;
import com.epoch.sys.service.ISysMenuService;

/**
 * 菜单权限
 * 
 * @author zhangqinglei
 * @date 2021/01/02
 * @qq 544188838
 */
import io.swagger.annotations.Api;

@RestController
@CrossOrigin
@Api(value = "菜单权限相关操作controller", tags = {"菜单权限操作接口"})
@RequestMapping("/epoch/sys/menu")
public class SysMenuController {
	
	@Autowired
	ISysMenuService sysMenuService;

	/**
	 * 
	 * @return
	 */
	@GetMapping(value = "/getMenuTree")
	public List<TreeNode> getMenuTree(){
		return sysMenuService.buildListToTree();
	}
}
