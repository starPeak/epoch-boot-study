package com.epoch.sys.common.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.epoch.sys.bean.SysDeptBean;
import com.epoch.sys.common.bean.DeptTreeNode;

public class DeptTreeUtil {

	public static List<DeptTreeNode> getDeptTreeList(List<SysDeptBean> deptList){
		//先找到第一层节点
		DeptTreeNode treeNode = null;
		for (SysDeptBean sysDeptBean : deptList) {
			if(sysDeptBean.getId() == 1) {
				treeNode = buildDeptTree(sysDeptBean);
				treeNode.setExpanded(false);
				break;
			}
		}
		if(null == treeNode) {
			return new ArrayList<DeptTreeNode>();
		}
		List<DeptTreeNode> childTreeList = getChildTree(treeNode.getDeptId(),deptList);
		for (DeptTreeNode deptTreeNode : childTreeList) {
			List<DeptTreeNode> childList = getChildTree(deptTreeNode.getDeptId(),deptList);
			if(!CollectionUtils.isEmpty(childList)) {
				deptTreeNode.setChildren(getChild(childList,deptList));
			}else {
				deptTreeNode.setLeaf(true);
			}
		}
		treeNode.setChildren(childTreeList);
		List<DeptTreeNode> list = new ArrayList<DeptTreeNode>();
		list.add(treeNode);
		return list;
	}
	
	private static List<DeptTreeNode> getChild(List<DeptTreeNode> childTreeList, List<SysDeptBean> deptList) {
		for (DeptTreeNode treeBean : childTreeList) {
			List<DeptTreeNode> childTreeList2 = getChildTree(treeBean.getDeptId(),deptList);
			if(childTreeList2.size()>0) {
				treeBean.setChildren(getChild(childTreeList2,deptList));
			}else {
				treeBean.setLeaf(true);
			}
		}
		return childTreeList;
	}

	private static List<DeptTreeNode> getChildTree(Integer deptId, List<SysDeptBean> deptList) {
		List<DeptTreeNode> childList = new ArrayList<DeptTreeNode>();
		for (SysDeptBean treeBean : deptList) {
			 if (treeBean.getParentId().equals(deptId)) {
				 childList.add(buildDeptTree(treeBean));
			 }
		}
		return childList;
	}

	private static DeptTreeNode buildDeptTree(SysDeptBean sysDeptBean) {
		DeptTreeNode deptTreeNode = new DeptTreeNode();
		deptTreeNode.setDeptId(sysDeptBean.getId());
		deptTreeNode.setCode(sysDeptBean.getCode());
		deptTreeNode.setCreateDate(sysDeptBean.getCreateDate());
		deptTreeNode.setName(sysDeptBean.getName());
		deptTreeNode.setStatus(sysDeptBean.getStatus());
		deptTreeNode.setParentId(sysDeptBean.getParentId());
		return deptTreeNode;
	}
}
