package com.epoch.sys.common.shiro.jwt;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */

import org.apache.shiro.web.filter.authc.BasicHttpAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;

import com.alibaba.fastjson.JSONObject;
import com.epoch.common.bean.ResponseBean;
import com.epoch.common.exception.BussinessException;
import com.epoch.sys.common.bean.JwtUserToken;
import com.epoch.sys.service.ISysUserService;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;

/**
 * JWT过滤
 */
public class JwtFilter extends BasicHttpAuthenticationFilter {

    /**
     * 需要放开的其他地址，可以配置
     */
    private static String[] loginUrl = {"/epoch/auth/login"};

    /**
     * logger
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtFilter.class);

    private ISysUserService sysUserService;

    /**
     * @param accountService
     */
    public JwtFilter(ISysUserService sysUserService) {
        super();
        this.sysUserService = sysUserService;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        HttpServletRequest httpRequest = WebUtils.toHttp(request);
        String url = httpRequest.getRequestURI();
        if (!url.startsWith("/epoch")) {
            return true;
        }
        // 判断用户是否想要登入
        if (Arrays.asList(loginUrl).contains(url)) {
            sysUserService.logout();
            return true;
        }
        // 获取权限
        String authorization = httpRequest.getHeader("Authorization");
        // 认证权限
        JwtUserToken token = sysUserService.verfiy(authorization);
        // 认证失败直接返回失败
        if (null == token) {
            response401(request, response, "凭据不存在或者已过期，请登录认证");
            return false;
        }
        return true;
    }

    /**
     * 这里我们详细说明下为什么重写 可以对比父类方法，只是将executeLogin方法调用去除了 如果没有去除将会循环调用doGetAuthenticationInfo方法
     */
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        // 未认证的情况上面已经处理 这里处理未授权
        Subject subject = SecurityUtils.getSubject();
        if (subject != null && subject.isAuthenticated()) {
            // 已经认证但未授权的情况
            // 告知客户端JWT没有权限访问此资源
            String data = JSONObject.toJSONString(new ResponseBean(HttpStatus.UNAUTHORIZED.value(), "未授权", null));
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=utf-8");
            PrintWriter printWriter;
            try {
                printWriter = WebUtils.toHttp(response).getWriter();
                printWriter.write(data);
                printWriter.flush();
            }catch (Exception e) {
                LOGGER.error(e.getMessage(),e);
            }
        }
        // 过滤链终止
        return false;
    }

    /**
     * 检测Header里面是否包含Authorization字段，有就进行Token登录认证授权
     */
    @Override
    protected boolean isLoginAttempt(ServletRequest request, ServletResponse response) {
        String token = this.getAuthzHeader(request);
        return token != null;
    }

    /**
     * 进行AccessToken登录认证授权
     */
    @Override
    protected boolean executeLogin(ServletRequest request, ServletResponse response) throws Exception {
        // 如果没有抛出异常则代表登入成功，返回true
        return true;
    }

    /**
     * 无需转发，直接返回Response信息
     */
    private void response401(ServletRequest req, ServletResponse resp, String msg) {
        HttpServletResponse httpServletResponse = (HttpServletResponse)resp;
        httpServletResponse.setStatus(HttpStatus.UNAUTHORIZED.value());
        httpServletResponse.setCharacterEncoding("UTF-8");
        httpServletResponse.setContentType("application/json; charset=utf-8");
        PrintWriter out = null;
        try {
            out = httpServletResponse.getWriter();
            String data = JSONObject.toJSONString(new ResponseBean(HttpStatus.UNAUTHORIZED.value(), msg, null));
            out.append(data);
            out.flush();
        } catch (IOException e) {
            LOGGER.error("直接返回Response信息出现IOException异常:" + e.getMessage());
            throw new BussinessException("直接返回Response信息出现IOException异常:" + e.getMessage());
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    /**
     * 对跨域提供支持
     */
    @Override
    protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest)request;
        HttpServletResponse httpServletResponse = (HttpServletResponse)response;
        httpServletResponse.setHeader("Access-control-Allow-Origin", httpServletRequest.getHeader("Origin"));
        httpServletResponse.setHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS,PUT,DELETE");
        httpServletResponse.setHeader("Access-Control-Allow-Headers",
            httpServletRequest.getHeader("Access-Control-Request-Headers"));
        // 跨域时会首先发送一个OPTIONS请求，这里我们给OPTIONS请求直接返回正常状态
        if (httpServletRequest.getMethod().equals(RequestMethod.OPTIONS.name())) {
            httpServletResponse.setStatus(HttpStatus.OK.value());
            return false;
        }
        return super.preHandle(request, response);
    }

}
