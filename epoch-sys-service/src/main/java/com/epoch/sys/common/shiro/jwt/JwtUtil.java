package com.epoch.sys.common.shiro.jwt;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */

import java.util.Date;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.net.util.Base64;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;
import com.epoch.sys.common.bean.JwtUserToken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Configuration
@Component("jwtUtil")
@ConditionalOnClass(Jwts.class)
public class JwtUtil {

    public static final String AUTHORIZATION_HEADER = "Bear ";

    public static final String JWT_SECRET = "7786df7fc3a34e26a61c034d5";

    public String generateToken(JwtUserToken userToken) {
        Date nowDate = new Date();
        Date expireDate = new Date(nowDate.getTime() + 60 * 60 * 60 * 1000);
        StringBuffer buffer = new StringBuffer();
        userToken.setExpireDate(expireDate.getTime());
        SecretKey key = generalKey();
        JwtBuilder builder = Jwts.builder();
        String subject = JSONObject.toJSONString(userToken);
        String token = builder.setSubject(subject).setIssuedAt(nowDate).setExpiration(expireDate)
            .signWith(SignatureAlgorithm.HS256, key).compact();
        buffer.append(AUTHORIZATION_HEADER);
        buffer.append(token);
        return buffer.toString();
    }
    
    public JwtUserToken verifyJwt(String token) {
        //签名秘钥，和生成的签名的秘钥一模一样
        SecretKey key = generalKey();
        Claims claims;
        try {
            token = token.replaceFirst(AUTHORIZATION_HEADER, "");
            claims = Jwts.parser()  //得到DefaultJwtParser
                    .setSigningKey(key)         //设置签名的秘钥
                    .parseClaimsJws(token).getBody();
            String userInfoStr = claims.getSubject();
            JwtUserToken userToken = JSONObject.parseObject(userInfoStr, JwtUserToken.class);
            return userToken;
        } catch (Exception e) {
            claims = null;
        }//设置需要解析的jwt
        return null;
    }

    public SecretKey generalKey() {
        byte[] encodedKey = Base64.decodeBase64(JWT_SECRET);// 本地的密码解码[B@152f6e2
        // 根据给定的字节数组使用AES加密算法构造一个密钥，使用
        // encodedKey中的始于且包含 0 到前 leng
        // 个字节这是当然是所有。（后面的文章中马上回推出讲解Java加密和解密的一些算法）
        SecretKey key = new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
        return key;
    }

//    public JwtUserToken getUser(String token) {
//        JwtParser jwtParser = Jwts.parser().setSigningKey(JWT_SECRET);
//        Claims body = jwtParser.parseClaimsJws(token.replace(AUTHORIZATION_HEADER, "")).getBody();
//        JSONObject jsonObject = JSONObject.fromObject(body.getSubject());
//        JwtUserToken user = getUser(jsonObject);
//        return user;
//    }
//
//    public JwtUserToken getUser(JSONObject jsonObject) {
//        JwtUserToken userToken = new JwtUserToken();
//        Integer userId = jsonObject.getInt("userId");
//        String account = jsonObject.getString("account");
//        String password = jsonObject.getString("password");
//        userToken.setAccount(account);
//        userToken.setUserId(userId);
//        userToken.setPassword(password);
//        return userToken;
//    }

}
