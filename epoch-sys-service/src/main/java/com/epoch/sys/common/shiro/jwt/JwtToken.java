package com.epoch.sys.common.shiro.jwt;

import org.apache.shiro.authc.HostAuthenticationToken;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */
public class JwtToken implements HostAuthenticationToken {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    
    private String account;
    
    private char[] password;

    /**
     * Token
     */
    private String token;

    /**
     * @param account
     * @param password
     */
    public JwtToken(String account, String password) {
         this(account,password.toCharArray());
    }
    
    public JwtToken(String account, char[] password) {
        this.account = account;
        this.password = password;
   }

    @Override
    public Object getPrincipal() {
        return account;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return password;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String getHost() {
         return null;
    }
}
