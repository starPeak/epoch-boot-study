package com.epoch.sys.common.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.ExpiredCredentialsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.epoch.common.bean.ResponseBean;
import com.epoch.common.exception.BaseExceptionAdvice;

/**
 * 异常控制处理器
 */
@RestControllerAdvice
public class ExceptionAdvice extends BaseExceptionAdvice {

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(IncorrectCredentialsException.class)
    public ResponseBean incorrectCredentials(IncorrectCredentialsException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "错误的凭证", null);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(UnknownAccountException.class)
    public ResponseBean unknownAccount(UnknownAccountException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "错误的帐号", null);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(LockedAccountException.class)
    public ResponseBean lockedAccount(LockedAccountException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "帐号已锁定", null);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ExcessiveAttemptsException.class)
    public ResponseBean excessiveAttempts(ExcessiveAttemptsException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "登录失败次数过多", null);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ExpiredCredentialsException.class)
    public ResponseBean expiredCredentials(ExpiredCredentialsException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "过期的凭证", null);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(DisabledAccountException.class)
    public ResponseBean disabledAccount(DisabledAccountException e) {
        return new ResponseBean(HttpStatus.INTERNAL_SERVER_ERROR.value(), "禁用的帐号", null);
    }
    
    /**
     * 捕捉其他所有异常
     * @param request
     * @param ex
     * @return
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ResponseBean globalException(HttpServletRequest request, Throwable ex) {
        return new ResponseBean(this.getStatus(request).value(), ex.toString() + ": " + ex.getMessage(), null);
    }

}
