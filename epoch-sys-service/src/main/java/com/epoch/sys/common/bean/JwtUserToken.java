package com.epoch.sys.common.bean;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */
public class JwtUserToken {

    private Integer userId;

    private String account;

    private String password;

    private Long expireDate;

    public JwtUserToken() {

    }

    public JwtUserToken(Integer userId, String account, String password) {
        this.userId = userId;
        this.account = account;
        this.password = password;
    }

    public JwtUserToken(Integer userId, String account, String password, Long expireDate) {
        this.userId = userId;
        this.account = account;
        this.password = password;
        this.expireDate = expireDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Long expireDate) {
        this.expireDate = expireDate;
    }

}
