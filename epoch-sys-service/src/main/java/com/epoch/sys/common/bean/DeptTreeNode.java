package com.epoch.sys.common.bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DeptTreeNode {

	private Integer deptId;
	
	private String name;
	
	private String code;
	
	private Integer parentId;
	
	private Integer status;
	
	private Date createDate;
	
	private boolean leaf = false;
	
	private Boolean expanded;
	
	private List<DeptTreeNode> children;

	public DeptTreeNode() {
		children = new ArrayList<DeptTreeNode>();
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Boolean getExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public List<DeptTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<DeptTreeNode> children) {
		this.children = children;
	}
	
}
