package com.epoch.sys.common.bean;

import java.util.List;

import com.epoch.sys.bean.SysMenuBean;

/**
 * 前端树节点
 * 
 * @author zhangqinglei
 * @date 2021/01/02
 * @qq 544188838
 */
public class TreeNode implements java.io.Serializable {

	private static final long serialVersionUID = 1L;

	private String menuId;

	private String text;
	
	private String textEn;

	private String parentId;

	private Integer type;

	private boolean leaf;

	private boolean expanded;

	private String iconCls;

	private String iconColor;

	private Boolean checked = null;

	private String link;

	private List<TreeNode> children;

	private int orderno;

	public TreeNode() {
	}

	public TreeNode(SysMenuBean sysMenuBean) {
		this.setMenuId(String.valueOf(sysMenuBean.getId()));
		this.setText(sysMenuBean.getMenuName());
		this.setLeaf(false);
		if (sysMenuBean.getMenuType() == 2) {
			this.setLeaf(true);
		}
		this.setParentId(String.valueOf(sysMenuBean.getParentId()));
		this.setIconCls(sysMenuBean.getMenuIcon());
		this.setLink(sysMenuBean.getMenuUrl());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public List<TreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<TreeNode> children) {
		this.children = children;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public String getIconColor() {
		return iconColor;
	}

	public void setIconColor(String iconColor) {
		this.iconColor = iconColor;
	}

	public int getOrderno() {
		return orderno;
	}

	public void setOrderno(int orderno) {
		this.orderno = orderno;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getTextEn() {
		return textEn;
	}

	public void setTextEn(String textEn) {
		this.textEn = textEn;
	}
}
