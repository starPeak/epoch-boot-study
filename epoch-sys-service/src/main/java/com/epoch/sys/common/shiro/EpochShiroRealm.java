package com.epoch.sys.common.shiro;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epoch.sys.bean.SysUserBean;
import com.epoch.sys.common.shiro.jwt.JwtToken;
import com.epoch.sys.common.util.AesCipherUtil;
import com.epoch.sys.service.ISysUserService;

@Component
public class EpochShiroRealm extends AuthorizingRealm {

    @Autowired
    private ISysUserService sysUserService;
    
    @Autowired
    private EhCacheManager cacheManager;

    public EpochShiroRealm() {
        this.setCacheManager(cacheManager);
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        principals.getPrimaryPrincipal();

        return null;
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        // 表示此Realm只支持JwtToken类型
        return token instanceof JwtToken;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
        throws AuthenticationException {
        JwtToken jwtToken = (JwtToken)authenticationToken;
        String account = (String)jwtToken.getPrincipal();
        // 帐号为空
        if (StringUtils.isBlank(account)) {
            throw new AuthenticationException("Token中帐号为空(The account in Token is empty.)");
        }
        SysUserBean accountBean = sysUserService.findSysUserByAccount(account);
        if (accountBean == null) {
            throw new AuthenticationException("该帐号不存在(The account does not exist.)");
        }
        String passwd = AesCipherUtil.deCrypto(accountBean.getPassword());
        return new SimpleAuthenticationInfo(accountBean.getAccount(),passwd.toCharArray(), getName());
    }

}
