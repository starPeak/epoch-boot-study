package com.epoch.sys.common.swagger;
// package com.epoch.example.swagger;
//
// /**
// * @author zhangqinglei
// * @date 2020/12/19
// * @qq 544188838
// */
//public class SecurityConfiguration {
//
//    private static final String[] AUTH_WHITELIST = {
//        // -- swagger ui
//        "/v2/api-docs",
//        "/swagger-resources",
//        "/swagger-resources/**",
//        "/configuration/ui",
//        "/configuration/security",
//        "/swagger-ui.html",
//        "/webjars/**"
//        // other public endpoints of your API may be appended to this array
//    };
//    
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        // TODO Auto-generated method stub
//        http.authorizeRequests()
//            .antMatchers("/v2/api-docs", "/swagger-resources/configuration/ui", "/swagger-resources", "/swagger-resources/configuration/security", "/swagger-ui.html", "/webjars/**").permitAll()
//            .and()
//            .authorizeRequests()
//            .anyRequest()
//            .authenticated()
//            .and()
//            .csrf().disable();
//    }
//}
