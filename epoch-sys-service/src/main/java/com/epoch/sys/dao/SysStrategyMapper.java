package com.epoch.sys.dao;

import com.epoch.sys.bean.SysStrategy;

public interface SysStrategyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(SysStrategy record);

    int insertSelective(SysStrategy record);

    SysStrategy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysStrategy record);

    int updateByPrimaryKey(SysStrategy record);
}