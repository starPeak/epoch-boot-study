package com.epoch.sys.dao;

import com.epoch.sys.bean.SysDictContent;

public interface SysDictContentMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysDictContent record);

    SysDictContent selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(SysDictContent record);
}