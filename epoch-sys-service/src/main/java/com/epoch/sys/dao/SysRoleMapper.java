package com.epoch.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.epoch.common.bean.ComFieldBean;
import com.epoch.sys.bean.SysRole;
import com.epoch.sys.bean.query.RoleQuery;

public interface SysRoleMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysRole record);

    SysRole selectByPrimaryKey(Integer id);

    int updateRole(SysRole record);
    
    List<SysRole> selectRoleList(RoleQuery query);
    
    List<ComFieldBean> getRoleComList();
    
    List<Integer> getRoleListByUserId(@Param("userId") Integer userId);
}