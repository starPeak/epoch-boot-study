package com.epoch.sys.dao;

import java.util.List;

import com.epoch.sys.bean.SysDict;
import com.epoch.sys.bean.query.DictQuery;

public interface SysDictMapper {
	
	List<SysDict> selectSysDictList(DictQuery query);
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysDict record);

    int insertSelective(SysDict record);

    SysDict selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(SysDict record);

}