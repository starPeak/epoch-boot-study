package com.epoch.sys.dao;

import java.util.List;

import com.epoch.sys.bean.SysDeptBean;

public interface SysDeptMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysDeptBean record);

    int insertSelective(SysDeptBean record);

    SysDeptBean selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysDeptBean record);

    int updateByPrimaryKey(SysDeptBean record);
    
    List<SysDeptBean> selectAll();
}