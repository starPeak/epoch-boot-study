package com.epoch.sys.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.epoch.sys.bean.SysUserBean;
import com.epoch.sys.bean.query.UserQuery;

/**
 * @author zhangqinglei
 * @date 2020/12/20
 * @qq 544188838
 */
public interface SysUserMapper {

    List<SysUserBean> selectAll();
    
    List<SysUserBean> selectUserList(UserQuery query);
    
    boolean deleteByPrimaryKey(Integer id);

    boolean insert(SysUserBean record);

    SysUserBean selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKey(SysUserBean record);
    
    boolean insertBatchAccount1(List<SysUserBean> accountList);
    
    SysUserBean findSysUserByAccount(String account);

	boolean updateLastUser(SysUserBean accountBean);

	void updateStatus(@Param("userId") Integer userId, @Param("status") Integer status);

	void resetPassword(@Param("userId") Integer userId, @Param("password") String password);
}
