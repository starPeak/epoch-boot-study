package com.epoch.sys.dao;

import com.epoch.sys.bean.SysUserRole;

public interface SysUserRoleMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysUserRole record);

    int insertSelective(SysUserRole record);

    SysUserRole selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysUserRole record);

    int updateByPrimaryKey(SysUserRole record);
    
    int deleteByUserId(Integer userId);

}