package com.epoch.sys.dao;

import java.util.List;

import com.epoch.sys.bean.SysMenuBean;

public interface SysMenuMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysMenuBean record);

    int insertSelective(SysMenuBean record);

    SysMenuBean selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(SysMenuBean record);

    int updateByPrimaryKey(SysMenuBean record);
    
    List<SysMenuBean> selectAll();
}