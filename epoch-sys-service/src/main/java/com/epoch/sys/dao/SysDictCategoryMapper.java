package com.epoch.sys.dao;

import java.util.List;

import com.epoch.sys.bean.SysDictCategory;
import com.epoch.sys.bean.query.DictQuery;

public interface SysDictCategoryMapper {
	
    int deleteByPrimaryKey(Integer id);

    int insert(SysDictCategory record);

    SysDictCategory selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(SysDictCategory record);
    
    List<SysDictCategory> selectSysDictCategoryList(DictQuery query);
    
    List<SysDictCategory> selectAll();
}