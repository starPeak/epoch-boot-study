package com.epoch.sys.service;

import com.epoch.common.bean.EpochTree;
import com.epoch.sys.bean.SysDict;
import com.epoch.sys.bean.SysDictCategory;
import com.epoch.sys.bean.query.DictQuery;
import com.github.pagehelper.PageInfo;

/**
 * 字典管理接口
 * @author zhangqinglei
 * @date 2021/01/23
 * @qq 544188838
 */
public interface ISysDistService {

	PageInfo<SysDict> selectDictList(DictQuery query);
	
	PageInfo<SysDictCategory> selectSysDictCategoryList(DictQuery query);

	EpochTree selectDictCategoryTree();
}
