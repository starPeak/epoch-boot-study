package com.epoch.sys.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epoch.common.bean.ComFieldBean;
import com.epoch.sys.bean.SysRole;
import com.epoch.sys.bean.query.RoleQuery;
import com.epoch.sys.dao.SysRoleMapper;
import com.epoch.sys.service.ISysRoleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class SysRoleServiceImpl implements ISysRoleService {

	@Autowired
	SysRoleMapper sysRoleMapper;

	@Override
	public PageInfo<SysRole> selectRoleList(RoleQuery query) {
		PageInfo<SysRole> pageInfo = PageHelper.startPage(query.getPage(), query.getLimit())
				.doSelectPageInfo(() -> sysRoleMapper.selectRoleList(query));
		return pageInfo;
	}

	@Override
	public SysRole selectByPrimaryKey(Integer id) {
		return sysRoleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int addRole(SysRole record) {
		record.setCreateDate(new Date());
		return sysRoleMapper.insert(record);
	}

	@Override
	public List<ComFieldBean> getRoleComList() {
		return sysRoleMapper.getRoleComList();
	}

	@Override
	public boolean deleteByIds(List<Integer> roleIds) {
		for (Integer roleId : roleIds) {
			sysRoleMapper.deleteByPrimaryKey(roleId);
		}
		return true;
	}

	@Override
	public boolean updateRole(SysRole sysRole) {
		sysRole.setUpdateDate(new Date());
		sysRole.setType(1);
		sysRoleMapper.updateRole(sysRole);
		return true;
	}

}
