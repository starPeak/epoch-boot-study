 package com.epoch.sys.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epoch.common.bean.EpochTree;
import com.epoch.sys.bean.SysDict;
import com.epoch.sys.bean.SysDictCategory;
import com.epoch.sys.bean.query.DictQuery;
import com.epoch.sys.bean.vo.DictCategoryTree;
import com.epoch.sys.dao.SysDictCategoryMapper;
import com.epoch.sys.dao.SysDictMapper;
import com.epoch.sys.service.ISysDistService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * 字典维护业务类
 * @author zhangqinglei
 * @date 2021/01/23
 * @qq 544188838
 */
@Service
public class SysDistServiceImpl implements ISysDistService{
	
	@Autowired
	SysDictMapper sDictMapper;
	
	@Autowired
	SysDictCategoryMapper sysDictCategoryMapper;

	@Override
	public PageInfo<SysDict> selectDictList(DictQuery query) {
		PageInfo<SysDict> pageInfo = PageHelper.startPage(query.getPage(), query.getLimit()).doSelectPageInfo(() -> sDictMapper.selectSysDictList(query));
        return pageInfo; 
	}

	@Override
	public PageInfo<SysDictCategory> selectSysDictCategoryList(DictQuery query) {
		PageInfo<SysDictCategory> pageInfo = PageHelper.startPage(query.getPage(), query.getLimit()).doSelectPageInfo(() -> sysDictCategoryMapper.selectSysDictCategoryList(query));
        return pageInfo; 
	}

	@Override
	public EpochTree selectDictCategoryTree() {
		EpochTree tree = new EpochTree("分组");
		List<DictCategoryTree> children = new ArrayList<DictCategoryTree>();
		List<SysDictCategory> allList = sysDictCategoryMapper.selectAll();
		for (SysDictCategory sysDictCategory : allList) {
			DictCategoryTree categoryTree = new DictCategoryTree();
			categoryTree.setExpanded(false);
			categoryTree.setLeaf(true);
			categoryTree.setText(sysDictCategory.getName());
			categoryTree.setTreeId(sysDictCategory.getId());
			children.add(categoryTree);
		}
		tree.setChildren(children);
		return tree;
	}
}
