package com.epoch.sys.service;

import java.util.List;

import com.epoch.sys.common.bean.TreeNode;

public interface ISysMenuService {

	public List<TreeNode> buildListToTree();
}
