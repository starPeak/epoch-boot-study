 package com.epoch.sys.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epoch.sys.bean.SysUserBean;
import com.epoch.sys.bean.query.UserQuery;
import com.epoch.sys.bean.vo.UserAuthorityVo;
import com.epoch.sys.common.bean.JwtUserToken;
import com.epoch.sys.common.bean.SysLoginDto;
import com.epoch.sys.common.bean.SysUserDataDto;
import com.github.pagehelper.PageInfo;

/**
 * @author zhangqinglei
 * @date 2020/12/20
 * @qq 544188838
 */
public interface ISysUserService {
    
    boolean deleteByPrimaryKey(Integer id);

    SysUserBean selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKeySelective(SysUserBean record);

    boolean updateByPrimaryKey(SysUserBean record);
    
    List<SysUserBean> selectAll();
    
    PageInfo<SysUserBean> selectUserList(UserQuery query);
    
    
    SysUserBean findSysUserByAccount(String account);

    /**
     * @param sysLoginDto
     * @return
     */
    SysUserDataDto login(SysLoginDto sysLoginDto);

    /**
     * @param authorization
     * @return
     */
    JwtUserToken verfiy(String authorization);
    
    public boolean logout();
    
    boolean allowLogOutJwt(HttpServletRequest httpRequest);
    
    public UserAuthorityVo getUserAuthority(Integer userId);

	SysUserBean getUserInfo(Integer userId);

	void addNewUser(SysUserBean userBean);

	void deleteByIds(List<Integer> userIds);

	void updateUser(SysUserBean userBean);

	void updateStatus(List<Integer> userIds,Integer status);

	void resetPassword(List<Integer> userIds);
}
