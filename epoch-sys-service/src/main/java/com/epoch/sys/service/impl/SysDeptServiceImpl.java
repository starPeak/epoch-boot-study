package com.epoch.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epoch.sys.common.bean.DeptTreeNode;
import com.epoch.sys.common.util.DeptTreeUtil;
import com.epoch.sys.dao.SysDeptMapper;
import com.epoch.sys.service.ISysDeptService;

@Service
public class SysDeptServiceImpl implements ISysDeptService{
	
	@Autowired
	SysDeptMapper sysDeptMapper;

	@Override
	public List<DeptTreeNode> getDeptTreeList() {
		 return DeptTreeUtil.getDeptTreeList(sysDeptMapper.selectAll());
	}

}
