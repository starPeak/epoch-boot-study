package com.epoch.sys.service;

import java.util.List;

import com.epoch.common.bean.ComFieldBean;
import com.epoch.sys.bean.SysRole;
import com.epoch.sys.bean.query.RoleQuery;
import com.github.pagehelper.PageInfo;

public interface ISysRoleService {

    PageInfo<SysRole> selectRoleList(RoleQuery query);
    
    SysRole selectByPrimaryKey(Integer id);
    
    int addRole(SysRole record);
    
    List<ComFieldBean> getRoleComList();

	boolean deleteByIds(List<Integer> roleIds);

	boolean updateRole(SysRole sysRole);
}
