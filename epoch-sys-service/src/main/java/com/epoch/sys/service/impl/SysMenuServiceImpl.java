package com.epoch.sys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epoch.sys.bean.SysMenuBean;
import com.epoch.sys.common.bean.TreeNode;
import com.epoch.sys.dao.SysMenuMapper;
import com.epoch.sys.service.ISysMenuService;

@Service
public class SysMenuServiceImpl implements ISysMenuService {

	@Autowired
	SysMenuMapper sysMenuMapper;

	/**
	 * 总共就有三级菜单 1-100为1级菜单 10000-90000为2级菜单 1000000-9000000为3级菜单
	 * 100000000-900000000为4级菜单
	 */
	@Override
	public List<TreeNode> buildListToTree() {
		List<SysMenuBean> allData = sysMenuMapper.selectAll();
		// 一级菜单
		Map<Integer, SysMenuBean> result = new HashMap<>();
		// 二级菜单
		Map<Integer, List<SysMenuBean>> submenuMap = new HashMap<>();
		for (SysMenuBean sysMenuBean : allData) {
			if (sysMenuBean.getId() <= 100) {
				result.put(sysMenuBean.getId(), sysMenuBean);
			}
			Integer parentId = sysMenuBean.getParentId();
			List<SysMenuBean> menuList = submenuMap.get(parentId);
			if (sysMenuBean.getId() >= 10000 && sysMenuBean.getId() <= 90000) {
				if (menuList != null) {
					menuList.add(sysMenuBean);
				} else {
					menuList = new ArrayList<SysMenuBean>();
					menuList.add(sysMenuBean);
				}
				submenuMap.put(parentId, menuList);
			}
			if (sysMenuBean.getId() >= 1000000 && sysMenuBean.getId() <= 9000000) {
				if (menuList != null) {
					menuList.add(sysMenuBean);
				} else {
					menuList = new ArrayList<SysMenuBean>();
					menuList.add(sysMenuBean);
				}
				submenuMap.put(parentId, menuList);
			}
			if (sysMenuBean.getId() >= 100000000 && sysMenuBean.getId() <= 900000000) {
				if (menuList != null) {
					menuList.add(sysMenuBean);
				} else {
					menuList = new ArrayList<SysMenuBean>();
					menuList.add(sysMenuBean);
				}
				submenuMap.put(parentId, menuList);
			}
		}
		List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
		for (Map.Entry<Integer, SysMenuBean> entry : result.entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
			TreeNode treeNode = new TreeNode(entry.getValue());
			List<SysMenuBean> menuList = submenuMap.get(entry.getKey());
			if (null != menuList) {
				List<TreeNode> subTreeList = new ArrayList<TreeNode>();
				for (SysMenuBean subMenu : menuList) {
					TreeNode subNode = new TreeNode(subMenu);
					List<SysMenuBean> submenuList = submenuMap.get(subMenu.getId());
					if (null != submenuList) {
						List<TreeNode> subTreeList2 = new ArrayList<TreeNode>();
						for (SysMenuBean subMenu2 : submenuList) {
							TreeNode subNode2 = new TreeNode(subMenu2);
							subTreeList2.add(subNode2);
						}
						subNode.setChildren(subTreeList2);
					}
					subTreeList.add(subNode);
				}
				treeNode.setChildren(subTreeList);
			}
			treeNodeList.add(treeNode);
		}
		return treeNodeList;
	}

}
