package com.epoch.sys.service;

import java.util.List;

import com.epoch.sys.common.bean.DeptTreeNode;

public interface ISysDeptService {

	public List<DeptTreeNode> getDeptTreeList();
}
