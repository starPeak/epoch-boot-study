 package com.epoch.sys.service.impl;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epoch.common.exception.BussinessException;
import com.epoch.common.utils.WebUtils;
import com.epoch.sys.bean.SysUserBean;
import com.epoch.sys.bean.SysUserRole;
import com.epoch.sys.bean.query.UserQuery;
import com.epoch.sys.bean.vo.UserAuthorityVo;
import com.epoch.sys.common.bean.JwtUserToken;
import com.epoch.sys.common.bean.SysLoginDto;
import com.epoch.sys.common.bean.SysUserDataDto;
import com.epoch.sys.common.shiro.jwt.JwtToken;
import com.epoch.sys.common.shiro.jwt.JwtUtil;
import com.epoch.sys.common.util.AesCipherUtil;
import com.epoch.sys.common.util.SysConstant;
import com.epoch.sys.dao.SysRoleMapper;
import com.epoch.sys.dao.SysUserMapper;
import com.epoch.sys.dao.SysUserRoleMapper;
import com.epoch.sys.service.ISysMenuService;
import com.epoch.sys.service.ISysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/**
 * @author zhangqinglei
 * @date 2020/12/20
 * @qq 544188838
 */
@Service
public class SysUserServiceImpl implements ISysUserService{
    
    @Autowired
    private JwtUtil jwtUtil;
    
    @Autowired
    private SysUserMapper sysUserMapper;
    
    @Autowired
    SysRoleMapper sysRoleMapper;
    
    @Autowired
    SysUserRoleMapper sysUserRoleMapper;
    
    @Autowired
    ISysMenuService sysMenuService;

    @Override
    public SysUserBean findSysUserByAccount(String account) {
         return sysUserMapper.findSysUserByAccount(account);
    }

    @Override
    public SysUserDataDto login(SysLoginDto sysLoginDto) {
        String account = sysLoginDto.getAccount();
        String password = sysLoginDto.getPassword();
        JwtToken jwtToken = new JwtToken(account, password);
        //登录认证，走token认证，如果错误，则直接返回不会继续下一步流程
        SecurityUtils.getSubject().login(jwtToken);
        //login成功则说明登录验证成功，然后返回token凭证
        SysUserBean accountBean = sysUserMapper.findSysUserByAccount(account);
        JwtUserToken userToken = new JwtUserToken(accountBean.getId(), sysLoginDto.getAccount(), sysLoginDto.getPassword());
        String ip = WebUtils.getIpAddress();
        accountBean.setLoginDate(new Date());
        accountBean.setLoginIp(ip);
        sysUserMapper.updateLastUser(accountBean);
        String token = jwtUtil.generateToken(userToken);
        SysUserDataDto userDto = new SysUserDataDto(account, token);
        userDto.setUserId(accountBean.getId());
        return userDto;
    }
    

    @Override
    public JwtUserToken verfiy(String authorization) {
        if(StringUtils.isBlank(authorization)) {
            return null;
        }
        JwtUserToken user = jwtUtil.verifyJwt(authorization);
        return user;
    }

    @Override
    public boolean logout() {
        Subject subject = SecurityUtils.getSubject();
        System.out.println("session:"+subject.getSession().getId());
        subject.logout();
        return true;
    }

    @Override
    public boolean allowLogOutJwt(HttpServletRequest httpRequest) {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return false;  
    }

    @Override
    public boolean deleteByPrimaryKey(Integer id) {
         return sysUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SysUserBean selectByPrimaryKey(Integer id) {
         return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public boolean updateByPrimaryKeySelective(SysUserBean record) {
         return sysUserMapper.updateByPrimaryKey(record);
    }

    @Override
    public boolean updateByPrimaryKey(SysUserBean record) {
        return sysUserMapper.updateByPrimaryKey(record);
    }
    
    @Override
    public List<SysUserBean> selectAll() {
         return sysUserMapper.selectAll();
    }

	@Override
	public PageInfo<SysUserBean> selectUserList(UserQuery query) {
		PageInfo<SysUserBean> pageInfo = PageHelper.startPage(query.getPage(), query.getLimit()).doSelectPageInfo(() -> sysUserMapper.selectUserList(query));
        return pageInfo; 
	}

	@Override
	public UserAuthorityVo getUserAuthority(Integer userId) {
		UserAuthorityVo vo = new UserAuthorityVo();
		vo.setMenuList(sysMenuService.buildListToTree());
		return vo;
	}

	@Override
	public SysUserBean getUserInfo(Integer userId) {
		SysUserBean userBean = sysUserMapper.selectByPrimaryKey(userId);
		List<Integer> roleList = sysRoleMapper.getRoleListByUserId(userId);
		userBean.setRoleList(roleList);
		return userBean;
	}

	@Override
	@Transactional
	public void addNewUser(SysUserBean accountBean) {
		String account = accountBean.getAccount();
		if(StringUtils.isBlank(account)) {
            throw new BussinessException("账号异常，不能为空");
        }
        SysUserBean queryuserBean = findSysUserByAccount(account);
        if (null != queryuserBean) {
            throw new BussinessException("账号已存在，不能重新注册");
        }
        String key = AesCipherUtil.enCrypto(accountBean.getPassword());
        accountBean.setPassword(key);
        accountBean.setAccount(account);
        accountBean.setCreateDate(new Date());
        sysUserMapper.insert(accountBean);
		List<Integer> roleList = accountBean.getRoleList();
		for (Integer roleId : roleList) {
			SysUserRole userRole = new SysUserRole(accountBean.getId(),roleId);
			sysUserRoleMapper.insert(userRole);
		}
	}

	@Override
	public void deleteByIds(List<Integer> userIds) {
		for (Integer userId : userIds) {
			if(SysConstant.MANAGE_ID == userId) {
				continue;
			}
			sysUserMapper.deleteByPrimaryKey(userId);
		}
	}

	@Override
	public void updateUser(SysUserBean userBean) {
		userBean.setUpdateDate(new Date());
		sysUserMapper.updateByPrimaryKey(userBean);
		List<Integer> roleList = userBean.getRoleList();
		sysUserRoleMapper.deleteByUserId(userBean.getId());
		for (Integer roleId : roleList) {
			sysUserRoleMapper.insert(new SysUserRole(userBean.getId(), roleId));
		}
	}

	@Override
	public void updateStatus(List<Integer> userIds,Integer status) {
		for (Integer userId : userIds) {
			if(!userId.equals(SysConstant.MANAGE_ID)) {
				sysUserMapper.updateStatus(userId,status);
			}
		}
	}

	@Override
	public void resetPassword(List<Integer> userIds) {
		for (Integer userId : userIds) {
			String password = AesCipherUtil.enCrypto(SysConstant.DEFAULT_PASSWORD);
			sysUserMapper.resetPassword(userId,password);
		}
	}

}
