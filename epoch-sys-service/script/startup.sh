#!/bin/bash

echo "============================================================================"
echo "========================epoch-sys-service start=========================="
echo "============================================================================"

CURRENT_DIR=$(cd `dirname $0`; pwd)
echo CURRENT_DIR:$CURRENT_DIR

CLASSPATH=.:$CURRENT_DIR/epoch-sys-service.jar:$CURRENT_DIR/config:$CURRENT_DIR/lib/*

echo CURRENT_DIR:$CURRENT_DIR
echo JAVA_HOME:$JAVA_HOME
echo CLASSPATH:$CLASSPATH

if [ "x$JAVA_HOME" != "x" ];then
    JAVA="$JAVA_HOME/bin/java"
else
	JAVA="java"
fi

JAVA_PARA=$1
if [ "x$JAVA_PARA" = "min" ];then
	JAVA_OPTS="-server -Xms512m -Xmx1024m -Xmn256m"
else
	JAVA_OPTS="-d64 -server"
fi
JAVA_OPTS="-server -Xms512m -Xmx1024m -Xmn256m"

JAVA_OPTS="$JAVA_OPTS -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=6810 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
JAVA_OPTS="$JAVA_OPTS -Duser.language=zh -Duser.region=CN -Duser.country=cn -Djava.security.policy=policy"


echo JAVA_OPTS:%JAVA_OPTS

$JAVA $JAVA_OPTS -cp $CLASSPATH com.epoch.sys.Application -DSpring.config.location=$CURRENT_DIR/config

