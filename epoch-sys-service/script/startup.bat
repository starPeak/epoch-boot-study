@echo off



echo "============================================================================"
echo "========================epoch-system-service start=========================="
echo "============================================================================"

setLocal EnableDelayedExpansion

set "CURRENT_DIR=%cd%"
echo CURRENT_DIR:%CURRENT_DIR%

set "JAVA_OPTS=-Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=9999 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
set "%JAVA_OPTS% -Duser.language=zh -Duser.region=CN -Duser.country=cn -Djava.security.policy=policy"

echo JAVA_OPTS:%JAVA_OPTS%

set "CLASSPATH=.;%CURRENT_DIR%\epoch-system-service.jar;%CURRENT_DIR%\lib\*"

echo CLASSPATH:%CLASSPATH%

set "EXECTOR=java %JAVA_OPTS% -cp %CLASSPATH% com.epoch.system.Application -DSpring.config.location=%CURRENT_DIR%/config"

%EXECTOR%