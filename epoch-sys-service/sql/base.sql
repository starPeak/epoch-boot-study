DROP DATABASE IF EXISTS `epoch-sys`;
CREATE DATABASE `epoch-sys`;
USE `epoch-sys`;

DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `LONG_NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL COMMENT '机构编码',
  `PARENT_ID` int(11) DEFAULT NULL,
  `ORDER` int(11) DEFAULT NULL COMMENT '排序',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态，1启用，0禁用',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10503 DEFAULT CHARSET=utf8;


INSERT INTO `sys_dept` (`ID`, `NAME`, `LONG_NAME`, `CODE`, `PARENT_ID`, `ORDER`, `STATUS`, `CREATE_DATE`) VALUES
	(1, '星峰企业', NULL, '1', 0, 1, 1, '2021-01-08 00:07:44'),
	(101, '财务部', NULL, '101', 1, 1, 1, '2021-01-08 00:07:44'),
	(102, '市场部', NULL, '102', 1, 2, 1, '2021-01-08 00:07:44'),
	(103, '行政部', NULL, '103', 1, 3, 1, '2021-01-08 00:07:44'),
	(104, '人力资源部', NULL, '104', 1, 4, 1, '2021-01-08 00:07:44'),
	(105, '研发部', NULL, '105', 1, 5, 1, '2021-01-08 00:07:44'),
	(10501, '研发一部', NULL, '10501', 105, 1, 1, '2021-01-08 00:07:44'),
	(10502, '研发二部', NULL, '10502', 105, 2, 1, '2021-01-08 00:07:44');

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `ID` int(11) NOT NULL,
  `MENU_NAME` varchar(255) DEFAULT NULL COMMENT '名字',
  `MENU_NAME_EN` varchar(255) DEFAULT NULL COMMENT '名字英文',
  `PARENT_ID` int(11) DEFAULT NULL,
  `MENU_ORDER` int(11) DEFAULT NULL COMMENT '排序',
  `MENU_URL` varchar(255) DEFAULT NULL COMMENT '主界面',
  `MENU_ICON` varchar(50) DEFAULT NULL COMMENT '图标',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态',
  `MENU_TYPE` int(11) DEFAULT NULL COMMENT '菜单类型1目录 2菜单',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sys_menu` (`ID`, `MENU_NAME`, `MENU_NAME_EN`, `PARENT_ID`, `MENU_ORDER`, `MENU_URL`, `MENU_ICON`, `STATUS`, `MENU_TYPE`) VALUES
	(1, '系统管理', 'SystemManage', 0, 1, NULL, 'iconfont icon-xitongguanli', 1, 1),
	(2, '系统监控', 'SystemMonitoring', 0, 2, NULL, 'iconfont icon-xitongjiankong', 1, 1),
	(3, '日志管理', 'LogManage', 0, 3, NULL, 'iconfont icon-rizhiguanli1', 1, 1),
	(10000, '系统用户', 'SystemUsers', 1, 1, 'Epoch.view.sys.user.SysUser', 'x-fa fa-user', 1, 2),
	(10001, '部门管理', NULL, 1, 2, 'Epoch.view.sys.dept.SysDept', 'iconfont icon-bumenguanli', 1, 2),
	(10002, '角色管理', NULL, 1, 3, 'Epoch.view.sys.role.SysRole', 'iconfont icon-jiaoseguanli', 1, 2),
	(10003, '员工管理', NULL, 1, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-yuangongguanli', 1, 2),
	(10004, '用户监控', NULL, 1, 5, 'Epoch.view.sys.user.SysUser', 'iconfont icon-zaixianyonghujiankong', 1, 2),
	(10005, '用户安全策略设置', NULL, 1, 6, 'Epoch.view.sys.user.SysUser', 'iconfont icon-anquanshezhi', 1, 2),
	(10006, '系统信息备份导入导出', NULL, 1, 7, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli3', 1, 2),
	(20000, '主机监控', NULL, 2, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-fuwuqi2', 1, 2),
	(20001, '服务监控', NULL, 2, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-zu1773', 1, 2),
	(20002, '数据库监控', NULL, 2, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shujukushenji', 1, 2),
	(20004, '缓存监控', NULL, 2, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-Redis', 1, 2),
	(20005, 'NTP管理', NULL, 2, 5, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shizhong', 1, 2),
	(20006, '日志管理', NULL, 2, 6, 'Epoch.view.sys.user.SysUser', 'iconfont icon-rizhiguanli1', 1, 2),
	(20007, '配置管理', NULL, 2, 7, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli', 1, 2),
	(30000, '操作日志查询', NULL, 3, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-caozuorizhi1', 1, 2),
	(30001, '用户日志查询', NULL, 3, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-rizhi', 1, 2),
	(30002, '日志备份', NULL, 3, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-beifen', 1, 2),
	(30003, '常规设置', NULL, 3, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shezhi', 1, 2),
	(2000601, '参数配置', NULL, 20006, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli1', 1, 2),
	(2000602, '级别调整', NULL, 20006, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-Level', 1, 2),
	(2000603, '日志下载', NULL, 20006, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-download', 1, 2),
	(2000604, '日志备份', NULL, 20006, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-0-28', 1, 2);

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE IF NOT EXISTS `sys_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `REMARK` text COMMENT '备注',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态，1启用，2禁用',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sys_role` (`ID`, `NAME`, `REMARK`, `STATUS`, `CREATE_DATE`, `UPDATE_DATE`) VALUES
	(1, '超级管理员', '超级管理员是系统内部自定义的权限，不能删除，是本系统最高权限的角色级。包含本系统所有的功能。', 1, '2021-01-10 01:00:02', '2021-01-10 01:00:26');

DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(11) NOT NULL,
  `MENU_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sys_set`;
CREATE TABLE `sys_set` (
  `ID` int(11) NOT NULL,
  `SET_TYPE` varchar(255) DEFAULT NULL,
  `SET_VALUE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `sys_set` (`ID`, `SET_TYPE`, `SET_VALUE`) VALUES
	(1, 'MAX_TAB', '8'),
	(2, 'SYS_NAME', 'Epoch微服务管理系统'),
	(3, 'SYS_VERSION', '1.0.0');

DROP TABLE IF EXISTS `sys_strategy`;
CREATE TABLE `sys_strategy` (
  `ID` int(11) NOT NULL,
  `STRATEGY_CODE` varchar(50) DEFAULT NULL,
  `STRATEGY_NAME` varchar(255) DEFAULT NULL,
  `STRATEGY_VALUE` varchar(255) DEFAULT NULL,
  `STRATEGY_TYPE` varchar(255) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO `sys_strategy` (`ID`, `STRATEGY_CODE`, `STRATEGY_NAME`, `STRATEGY_VALUE`, `STRATEGY_TYPE`, `CREATE_DATE`) VALUES
	(1, '1', '密码长度范围', '6-12', 'PASSWORD', '2021-01-10 00:28:23'),
	(2, '2', '密码重复次数', '10', 'PASSWORD', '2021-01-10 00:28:23'),
	(3, '3', '密码组成类型', '2', 'PASSWORD', '2021-01-10 00:30:34'),
	(4, '4', '定期修改密码天数', '60', 'PASSWORD', '2021-01-10 00:31:29'),
	(5, '5', '登录失败几次后锁定', '5', 'ACCOUNT', '2021-01-10 00:31:29'),
	(6, '6', '自动解锁时间(分钟)', '30', 'ACCOUNT', '2021-01-10 00:31:29'),
	(7, '7', '闲置用户多久锁定(天数)', '30', 'ACCOUNT', '2021-01-10 00:31:29');

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNT` varchar(255) DEFAULT NULL COMMENT '账号',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `REAL_NAME` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(255) DEFAULT NULL COMMENT '手机',
  `STATUS` int(2) DEFAULT NULL COMMENT '状态，1启用，2锁定，0删除',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建日期',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后一次修改日期',
  `LOGIN_DATE` datetime DEFAULT NULL COMMENT '登录日期',
  `DEPT_ID` int(11) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;


INSERT INTO `sys_user` (`ID`, `ACCOUNT`, `PASSWORD`, `REAL_NAME`, `EMAIL`, `PHONE`, `STATUS`, `CREATE_DATE`, `UPDATE_DATE`, `LOGIN_DATE`, `DEPT_ID`) VALUES
	(116, 'admin', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', NULL, NULL);

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


INSERT INTO `sys_user_role` (`ID`, `USER_ID`, `ROLE_ID`) VALUES
	(1, 116, 1);
