-- --------------------------------------------------------
-- 主机:                           localhost
-- 服务器版本:                        5.7.32-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.1.0.6116
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 epoch-sys 的数据库结构
CREATE DATABASE IF NOT EXISTS `epoch-sys` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epoch-sys`;

-- 导出  表 epoch-sys.sys_dept 结构
CREATE TABLE IF NOT EXISTS `sys_dept` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) DEFAULT NULL COMMENT '名称',
  `LONG_NAME` varchar(255) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL COMMENT '机构编码',
  `PARENT_ID` int(11) DEFAULT NULL,
  `ORDER` int(11) DEFAULT NULL COMMENT '排序',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态，1启用，0禁用',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10503 DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_dept 的数据：~7 rows (大约)
/*!40000 ALTER TABLE `sys_dept` DISABLE KEYS */;
INSERT INTO `sys_dept` (`ID`, `NAME`, `LONG_NAME`, `CODE`, `PARENT_ID`, `ORDER`, `STATUS`, `CREATE_DATE`) VALUES
	(1, '星峰企业', NULL, '1', 0, 1, 1, '2021-01-08 00:07:44'),
	(101, '财务部', NULL, '101', 1, 1, 1, '2021-01-08 00:07:44'),
	(102, '市场部', NULL, '102', 1, 2, 1, '2021-01-08 00:07:44'),
	(103, '行政部', NULL, '103', 1, 3, 1, '2021-01-08 00:07:44'),
	(104, '人力资源部', NULL, '104', 1, 4, 1, '2021-01-08 00:07:44'),
	(105, '研发部', NULL, '105', 1, 5, 1, '2021-01-08 00:07:44'),
	(10501, '研发一部', NULL, '10501', 105, 1, 1, '2021-01-08 00:07:44'),
	(10502, '研发二部', NULL, '10502', 105, 2, 1, '2021-01-08 00:07:44');
/*!40000 ALTER TABLE `sys_dept` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_dict 结构
CREATE TABLE IF NOT EXISTS `sys_dict` (
  `ID` int(11) NOT NULL,
  `DICT_CODE` varchar(255) DEFAULT NULL,
  `DICT_CATEGORY_ID` int(11) DEFAULT NULL COMMENT '关联分组ID',
  `DICT_NAME` varchar(255) DEFAULT NULL,
  `DICT_COMMENTS` text,
  `DICT_FLAG` int(11) DEFAULT NULL COMMENT '状态是否启用，1启用，0禁用',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_dict 的数据：~1 rows (大约)
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` (`ID`, `DICT_CODE`, `DICT_CATEGORY_ID`, `DICT_NAME`, `DICT_COMMENTS`, `DICT_FLAG`, `CREATE_DATE`, `UPDATE_DATE`) VALUES
	(1, 'BS_GENDER', 1, '性别', '性别', 1, '2021-01-24 15:17:01', NULL);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_dict_category 结构
CREATE TABLE IF NOT EXISTS `sys_dict_category` (
  `ID` int(11) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `REMARK` text,
  `TYPE` int(11) DEFAULT NULL COMMENT '0内置，1其他',
  `STATUS` int(11) DEFAULT NULL COMMENT '1正常，2已锁定',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_dict_category 的数据：~2 rows (大约)
/*!40000 ALTER TABLE `sys_dict_category` DISABLE KEYS */;
INSERT INTO `sys_dict_category` (`ID`, `NAME`, `REMARK`, `TYPE`, `STATUS`, `CREATE_DATE`, `UPDATE_DATE`) VALUES
	(1, '通用业务组', '这里存放通用的数据字典分类', 0, 1, '2021-01-23 10:07:56', NULL),
	(2, '附件相关组', '这里存放和文件相关的业务', 0, 1, '2021-01-23 10:07:56', NULL);
/*!40000 ALTER TABLE `sys_dict_category` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_dict_content 结构
CREATE TABLE IF NOT EXISTS `sys_dict_content` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `DICT_ID` int(11) NOT NULL COMMENT '和SYS_DICT关联',
  `ITEM_CODE` varchar(255) DEFAULT NULL COMMENT '编码值',
  `ITEM_NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `ITEM_SEQ` int(11) DEFAULT NULL COMMENT '排序',
  `ITEM_FLAG` int(11) DEFAULT NULL COMMENT '状态是否启用，1启用，0禁用',
  `CREATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_dict_content 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_dict_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_dict_content` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_login_history 结构
CREATE TABLE IF NOT EXISTS `sys_login_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_NAME` varchar(255) DEFAULT NULL COMMENT '登录账户',
  `LOGIN_IP` varchar(255) DEFAULT NULL COMMENT '登录IP',
  `LOGIN_IDENT` varchar(255) DEFAULT NULL COMMENT '登录标识',
  `LOGIN_DATE` datetime DEFAULT NULL COMMENT '登录时间',
  `LOGOUT_DATE` datetime DEFAULT NULL COMMENT '登出时间',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_login_history 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_login_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_login_history` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_menu 结构
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `ID` int(11) NOT NULL,
  `MENU_NAME` varchar(255) DEFAULT NULL COMMENT '名字',
  `MENU_NAME_EN` varchar(255) DEFAULT NULL COMMENT '名字英文',
  `PARENT_ID` int(11) DEFAULT NULL,
  `MENU_ORDER` int(11) DEFAULT NULL COMMENT '排序',
  `MENU_URL` varchar(255) DEFAULT NULL COMMENT '主界面',
  `MENU_ICON` varchar(50) DEFAULT NULL COMMENT '图标',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态',
  `MENU_TYPE` int(11) DEFAULT NULL COMMENT '菜单类型1目录 2菜单',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_menu 的数据：~28 rows (大约)
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` (`ID`, `MENU_NAME`, `MENU_NAME_EN`, `PARENT_ID`, `MENU_ORDER`, `MENU_URL`, `MENU_ICON`, `STATUS`, `MENU_TYPE`) VALUES
	(1, '系统管理', 'SystemManage', 0, 1, NULL, 'iconfont icon-xitongguanli', 1, 1),
	(2, '系统监控', 'SystemMonitoring', 0, 2, NULL, 'iconfont icon-xitongjiankong', 1, 1),
	(3, '日志管理', 'LogManage', 0, 3, NULL, 'iconfont icon-rizhiguanli1', 1, 1),
	(10000, '系统用户', 'SystemUsers', 1, 1, 'Epoch.view.sys.user.SysUser', 'x-fa fa-user', 1, 2),
	(10001, '部门管理', NULL, 1, 2, 'Epoch.view.sys.dept.SysDept', 'iconfont icon-bumenguanli', 1, 2),
	(10002, '角色管理', NULL, 1, 3, 'Epoch.view.sys.role.SysRole', 'iconfont icon-jiaoseguanli', 1, 2),
	(10003, '员工管理', NULL, 1, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-yuangongguanli', 1, 2),
	(10004, '用户监控', NULL, 1, 5, 'Epoch.view.sys.user.SysUser', 'iconfont icon-zaixianyonghujiankong', 1, 2),
	(10005, '用户安全策略设置', NULL, 1, 6, 'Epoch.view.sys.user.SysUser', 'iconfont icon-anquanshezhi', 1, 2),
	(10006, '系统信息备份导入导出', NULL, 1, 7, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli3', 1, 2),
	(10007, '数据字典', NULL, 1, 8, 'Epoch.view.sys.user.SysUser', 'x-fa fa-book', 1, 1),
	(20000, '主机监控', NULL, 2, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-fuwuqi2', 1, 2),
	(20001, '服务监控', NULL, 2, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-zu1773', 1, 2),
	(20002, '数据库监控', NULL, 2, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shujukushenji', 1, 2),
	(20004, '缓存监控', NULL, 2, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-Redis', 1, 2),
	(20005, 'NTP管理', NULL, 2, 5, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shizhong', 1, 2),
	(20006, '日志管理', NULL, 2, 6, 'Epoch.view.sys.user.SysUser', 'iconfont icon-rizhiguanli1', 1, 2),
	(20007, '配置管理', NULL, 2, 7, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli', 1, 2),
	(30000, '操作日志查询', NULL, 3, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-caozuorizhi1', 1, 2),
	(30001, '用户日志查询', NULL, 3, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-rizhi', 1, 2),
	(30002, '日志备份', NULL, 3, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-beifen', 1, 2),
	(30003, '常规设置', NULL, 3, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-shezhi', 1, 2),
	(1000701, '数据字典分组', NULL, 10007, 1, 'Epoch.view.sys.dict.category.SysDictCategory', 'iconfont icon-shujuzidianfenlei', 1, 2),
	(1000702, '数据字典', NULL, 10007, 2, 'Epoch.view.sys.dict.SysDict', 'iconfont icon-shujuzidian', 1, 2),
	(2000601, '参数配置', NULL, 20006, 1, 'Epoch.view.sys.user.SysUser', 'iconfont icon-peizhiguanli1', 1, 2),
	(2000602, '级别调整', NULL, 20006, 2, 'Epoch.view.sys.user.SysUser', 'iconfont icon-Level', 1, 2),
	(2000603, '日志下载', NULL, 20006, 3, 'Epoch.view.sys.user.SysUser', 'iconfont icon-download', 1, 2),
	(2000604, '日志备份', NULL, 20006, 4, 'Epoch.view.sys.user.SysUser', 'iconfont icon-0-28', 1, 2);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL COMMENT '名称',
  `REMARK` text COMMENT '备注',
  `STATUS` int(11) DEFAULT NULL COMMENT '状态，1启用，2禁用',
  `TYPE` int(11) DEFAULT NULL COMMENT '0内置权限，1其他',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_role 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` (`ID`, `NAME`, `REMARK`, `STATUS`, `TYPE`, `CREATE_DATE`, `UPDATE_DATE`) VALUES
	(1, '超级管理员', '内置权限用户，超级管理员是系统内部自定义的权限，不能删除，是本系统最高权限的角色级。包含本系统所有的功能。', 1, 0, '2021-01-10 01:00:02', '2021-01-10 01:00:26'),
	(2, '普通查询用户', '内置权限用户，具有除系统管理外其他的查询权限。', 1, 0, '2021-01-10 01:00:02', '2021-01-10 01:00:26'),
	(3, '系统用户管理', '内置权限用户，仅具有系统管理下的权限', 1, 0, '2021-01-10 01:00:02', '2021-01-10 01:00:26');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_role_menu 结构
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_ID` int(11) NOT NULL,
  `MENU_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_role_menu 的数据：~0 rows (大约)
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_set 结构
CREATE TABLE IF NOT EXISTS `sys_set` (
  `ID` int(11) NOT NULL,
  `SET_TYPE` varchar(255) DEFAULT NULL,
  `SET_VALUE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_set 的数据：~3 rows (大约)
/*!40000 ALTER TABLE `sys_set` DISABLE KEYS */;
INSERT INTO `sys_set` (`ID`, `SET_TYPE`, `SET_VALUE`) VALUES
	(1, 'MAX_TAB', '8'),
	(2, 'SYS_NAME', 'Epoch微服务管理系统'),
	(3, 'SYS_VERSION', '1.0.0');
/*!40000 ALTER TABLE `sys_set` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_strategy 结构
CREATE TABLE IF NOT EXISTS `sys_strategy` (
  `ID` int(11) NOT NULL,
  `STRATEGY_CODE` varchar(50) DEFAULT NULL,
  `STRATEGY_NAME` varchar(255) DEFAULT NULL,
  `STRATEGY_VALUE` varchar(255) DEFAULT NULL,
  `STRATEGY_TYPE` varchar(255) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_strategy 的数据：~8 rows (大约)
/*!40000 ALTER TABLE `sys_strategy` DISABLE KEYS */;
INSERT INTO `sys_strategy` (`ID`, `STRATEGY_CODE`, `STRATEGY_NAME`, `STRATEGY_VALUE`, `STRATEGY_TYPE`, `CREATE_DATE`) VALUES
	(1, '1', '密码长度范围', '6-12', 'PASSWORD', '2021-01-10 00:28:23'),
	(2, '2', '密码重复次数', '10', 'PASSWORD', '2021-01-10 00:28:23'),
	(3, '3', '密码组成类型', '2', 'PASSWORD', '2021-01-10 00:30:34'),
	(4, '4', '定期修改密码天数', '60', 'PASSWORD', '2021-01-10 00:31:29'),
	(5, '5', '登录失败几次后锁定', '5', 'ACCOUNT', '2021-01-10 00:31:29'),
	(6, '6', '自动解锁时间(分钟)', '30', 'ACCOUNT', '2021-01-10 00:31:29'),
	(7, '7', '闲置用户多久锁定(天数)', '30', 'ACCOUNT', '2021-01-10 00:31:29');
/*!40000 ALTER TABLE `sys_strategy` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACCOUNT` varchar(255) DEFAULT NULL COMMENT '账号',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `REAL_NAME` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `EMAIL` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `PHONE` varchar(255) DEFAULT NULL COMMENT '手机',
  `STATUS` int(2) DEFAULT NULL COMMENT '状态，1启用，2锁定，0删除',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建日期',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后一次修改日期',
  `LOGIN_DATE` datetime DEFAULT NULL COMMENT '登录日期',
  `LOGIN_IP` varchar(255) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_user 的数据：~36 rows (大约)
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`ID`, `ACCOUNT`, `PASSWORD`, `REAL_NAME`, `EMAIL`, `PHONE`, `STATUS`, `CREATE_DATE`, `UPDATE_DATE`, `LOGIN_DATE`, `LOGIN_IP`) VALUES
	(116, 'admin', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-24 05:13:38', '192.168.20.1'),
	(117, 'a0001', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰1', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:30', '2021-01-20 11:46:06', '192.168.20.1'),
	(118, 'a0002', 'NzYzNkNGRDE2RjFBRjkyRkZBRkM5MzRDQ0RDQURFQjI=', '汪峰2', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:34', '2021-01-20 11:46:06', '192.168.20.1'),
	(119, 'a0003', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰3', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:37', '2021-01-20 11:46:06', '192.168.20.1'),
	(120, 'a0004', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰4', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:44', '2021-01-20 11:46:06', '192.168.20.1'),
	(121, 'a0005', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰5', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:50', '2021-01-20 11:46:06', '192.168.20.1'),
	(122, 'a0006', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰6', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:56', '2021-01-20 11:46:06', '192.168.20.1'),
	(123, 'a0007', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰7', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:15:03', '2021-01-20 11:46:06', '192.168.20.1'),
	(124, 'a0008', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(125, 'a0009', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(126, 'a0010', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(127, 'a0011', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(128, 'a0012', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(129, 'a0013', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(130, 'a0014', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(131, 'a0015', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(132, 'a0016', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(133, 'a0017', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(134, 'a0018', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(135, 'a0019', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(136, 'a0020', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(137, 'a0021', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(138, 'a0022', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(139, 'a0023', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(140, 'a0024', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(141, 'a0025', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(142, 'a0026', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(143, 'a0027', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(144, 'a0028', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(145, 'a0029', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(146, 'a0030', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(147, 'a0031', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(148, 'a0032', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(149, 'a0033', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2020-12-20 15:07:20', '2021-01-20 11:46:06', '192.168.20.1'),
	(150, 'a0034', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰34', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:14:13', '2021-01-20 11:46:06', '192.168.20.1'),
	(151, 'a0035', 'OUM5Q0UxMUEwMzRGNkI2OTdFNUJFQkZBRDc1MzZFRDQ=', '汪峰35', '544188838@qq.com', '1234561', 1, '2020-12-18 06:24:10', '2021-01-24 07:13:58', '2021-01-20 11:46:06', '192.168.20.1');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;

-- 导出  表 epoch-sys.sys_user_role 结构
CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`) USING BTREE,
  KEY `ID` (`ID`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-sys.sys_user_role 的数据：~19 rows (大约)
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` (`ID`, `USER_ID`, `ROLE_ID`) VALUES
	(2, 11, 2),
	(3, 27, 1),
	(4, 28, 1),
	(5, 29, 2),
	(6, 30, 4),
	(7, 31, 1),
	(8, 32, 4),
	(9, 33, 4),
	(10, 116, 1),
	(17, 34, 4),
	(21, 151, 3),
	(22, 150, 1),
	(23, 117, 1),
	(24, 118, 2),
	(25, 119, 1),
	(26, 120, 1),
	(27, 121, 1),
	(28, 122, 1),
	(29, 123, 1);
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
