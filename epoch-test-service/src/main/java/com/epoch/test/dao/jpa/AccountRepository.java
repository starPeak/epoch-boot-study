package com.epoch.test.dao.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.epoch.test.bean.jpa.Account;

public interface AccountRepository extends JpaRepository<Account, Integer>{

}
