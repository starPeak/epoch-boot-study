package com.epoch.test.dao.mybatis;

import java.util.List;

import com.epoch.test.bean.mybatis.AccountBean;
/**
 * 账号信息查询数据库访问层
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
public interface AccountMapper {
    
    List<AccountBean> selectAll();
    
    boolean deleteByPrimaryKey(Integer id);

    boolean insert(AccountBean record);

    AccountBean selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKeySelective(AccountBean record);

    boolean updateByPrimaryKey(AccountBean record);
    
    boolean insertBatchAccount1(List<AccountBean> accountList);
    
    AccountBean findSysUserByAccount(String account);

}