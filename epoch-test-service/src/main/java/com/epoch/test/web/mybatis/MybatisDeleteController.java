package com.epoch.test.web.mybatis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.test.dao.mybatis.AccountMapper;
import com.epoch.test.service.activerecord.IActiveRecordAccountService;
import com.epoch.test.service.mybatis.IMybatisAccountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@CrossOrigin
@Api(value="Mybaits相关操作controller",tags={"Mybaits操作接口"})
@RestController
@RequestMapping("/epoch/study/mybatis")
public class MybatisDeleteController {
    
    @Autowired
    IMybatisAccountService accountService;
    
    @Autowired
    AccountMapper accountMapper;
    
    /**
     * 示例1,ID在URL拼接中，ID默认是必须加的
     * @param id
     * @return
     */
    @ApiOperation(value="根据用户ID删除用户", notes="根据用户ID删除用户")
    @DeleteMapping(path = "/delete/{id}")
    public boolean deleteAccountById1(@ApiParam(name="id",value="用户id",example="-1",required=true) @PathVariable Integer id){
        boolean result = accountService.deleteByPrimaryKey(id);
        return result; 
    }
    
    /**
     * 示例2,ID在请求中参数携带
     * @param id
     * @return
     */
    @ApiOperation(value="根据用户ID删除用户", notes="根据用户ID删除用户")
    @DeleteMapping(path = "/delete")
    public boolean deleteAccountById2(@ApiParam(name="id",value="用户id",example="-1",required=true) @RequestParam(required = true) Integer id){
        boolean result = accountService.deleteByPrimaryKey(id);
        return result; 
    }
    
    /**
     * 示例3
     * @param id
     * @return
     */
    @ApiOperation(value="根据用户ID删除用户", notes="根据用户ID删除用户")
    @DeleteMapping(path = "/deleteByIds")
    public boolean deleteByIds(@RequestBody List<Integer> ids){
        for (Integer id : ids) {
            accountService.deleteByPrimaryKey(id);
        }
        return true; 
    }
    
}
