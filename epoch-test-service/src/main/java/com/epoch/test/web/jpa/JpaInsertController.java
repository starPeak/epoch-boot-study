package com.epoch.test.web.jpa;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.test.bean.jpa.Account;
import com.epoch.test.service.jpa.IJpaAccountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * JPA插入案例
 * @author zhangqinglei
 * @date 2020/12/14
 * @qq 544188838
 */
@CrossOrigin
@Api(value="jpa相关操作controller",tags={"jpa操作接口"})
@RestController
@RequestMapping("/epoch/study/jpa")
public class JpaInsertController {
    
    @Autowired
    IJpaAccountService accountService;

    /**
     * AJAX 请求，使用请求携带JSON
     * @param accountBean
     * @return
     */
    @ApiOperation(value="用户增加", notes="根据用户信息进行保存")
    @PostMapping(path = "/insertAccount")
    public boolean insertAccount(@RequestBody @Valid Account account) {
        boolean result = accountService.insert(account);
        return result; 
    }

}
