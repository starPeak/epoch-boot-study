package com.epoch.test.web.mybatis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.test.bean.mybatis.AccountBean;
import com.epoch.test.bean.mybatis.query.AccountQuery;
import com.epoch.test.dao.mybatis.AccountMapper;
import com.epoch.test.service.activerecord.IActiveRecordAccountService;
import com.epoch.test.service.mybatis.IMybatisAccountService;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * Mybatis查询案例，包含分页
 * 
 * 分页参考文档：https://github.com/pagehelper/Mybatis-PageHelper/blob/master/wikis/zh/HowToUse.md
 * @author zhangqinglei
 * @date 2020/12/10
 * @qq 544188838
 */
@CrossOrigin
@Api(value="Mybaits相关操作controller",tags={"Mybaits操作接口"})
@RestController
@RequestMapping("/epoch/study/mybatis")
public class MybatisQueryController {
    
    @Autowired
    IMybatisAccountService accountService;
    
    @Autowired
    AccountMapper accountMapper;
    
    /**
     * 示例1,ID在URL拼接中，ID默认是必须加的
     * @param id
     * @return
     */
    @ApiOperation(value="根据用户ID查询用户信息", notes="根据用户ID查询用户信息")
    @GetMapping(path = "/select/{id}")
    public AccountBean selectAccountById1(@ApiParam(name="id",value="用户id",example="-1",required=true) @PathVariable Integer id){
        AccountBean accountBean = accountService.selectByPrimaryKey(id);
        return accountBean; 
    }
    
    /**
     * 示例2,ID在请求中参数携带
     * @param id
     * @return
     */
    @ApiOperation(value="根据用户ID查询用户信息", notes="根据用户ID查询用户信息")
    @GetMapping(path = "/select")
    public AccountBean selectAccountById2(@ApiParam(name="id",value="用户id",example="-1",required=true) @RequestParam(required = true) Integer id){
        AccountBean accountBean = accountService.selectByPrimaryKey(id);
        return accountBean; 
    }
    
    /**
     * 查询所有的用户信息
     * @param id
     * @return
     */
    @ApiOperation(value="查询所有的用户信息", notes="查询所有的用户信息")
    @GetMapping(path = "/selectAll")
    public List<AccountBean> selectAll(){
        List<AccountBean> accountBeanList = accountService.selectAll();
        return accountBeanList; 
    }
    
    /**
     * 分页案例1：
     * 查询所有的用户信息,分页展示
     * @param id
     * @return
     */
    @ApiOperation(value="查询所有的用户信息", notes="查询所有的用户信息")
    @PostMapping(path = "/selectByPage1")
    public Page<AccountBean> selectByPage1(@RequestBody AccountQuery accountQuery){
        Page<AccountBean> pageData = PageHelper.startPage(accountQuery.getPage(), accountQuery.getPageSize()).doSelectPage(()-> accountMapper.selectAll());
        return pageData; 
    }
    
    /**
     * 分页案例2：
     * 查询所有的用户信息,分页展示
     * @param id
     * @return
     */
    @ApiOperation(value="查询所有的用户信息", notes="查询所有的用户信息")
    @PostMapping(path = "/selectByPage2")
    public Page<AccountBean> selectByPage2(@RequestBody AccountQuery accountQuery){
        Page<AccountBean> page = PageHelper.startPage(accountQuery.getPage(), accountQuery.getPageSize()).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                accountMapper.selectAll();
            }
        });
        return page;
    }
    
    /**
     * 分页案例3：
     * 查询所有的用户信息,分页展示
     * @param id
     * @return
     */
    @ApiOperation(value="查询所有的用户信息", notes="查询所有的用户信息")
    @PostMapping(path = "/selectByPage3")
    public PageInfo<AccountBean> selectByPage3(@RequestBody AccountQuery accountQuery){
        PageInfo<AccountBean> pageInfo = PageHelper.startPage(accountQuery.getPage(), accountQuery.getPageSize()).doSelectPageInfo(() -> accountMapper.selectAll());
        return pageInfo; 
    }
    
    /**
     * 分页案例4：
     * 查询所有的用户信息,分页展示
     * @param id
     * @return
     */
    @ApiOperation(value="查询所有的用户信息", notes="查询所有的用户信息")
    @PostMapping(path = "/selectByPage4")
    public PageInfo<AccountBean> selectByPage4(@RequestBody AccountQuery accountQuery){
        PageInfo<AccountBean> pageInfo = PageHelper.startPage(accountQuery.getPage(), accountQuery.getPageSize()).doSelectPageInfo(new ISelect() {
            @Override
            public void doSelect() {
                accountMapper.selectAll();
            }
        });
        return pageInfo; 
    }
}
