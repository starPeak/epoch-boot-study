package com.epoch.test.web.mybatis;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.test.bean.mybatis.AccountBean;
import com.epoch.test.dao.mybatis.AccountMapper;
import com.epoch.test.service.mybatis.IMybatisAccountService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Mybatis插入案例
 * 演示了如下
 * 1.单挑数据插入
 * 2.多条数据插入
 * 3.大量数据，建议1万以上使用，使用Mybatis的batch模式插入，原理为JDBC的batch模式
 * @author zhangqinglei
 * @date 2020/12/10
 * @qq 544188838
 */
@CrossOrigin
@Api(value="Mybaits相关操作controller",tags={"Mybaits操作接口"})
@RestController
@RequestMapping("/epoch/study/mybatis")
public class MybatisInsertController {
    
    @Autowired
    IMybatisAccountService accountService;
    
    @Autowired
    AccountMapper accountMapper;

    /**
     * AJAX 请求，使用请求携带JSON
     * @param accountBean
     * @return
     */
    @ApiOperation(value="用户增加", notes="根据用户信息进行保存")
    @PostMapping(path = "/insertAccount")
    public boolean insertAccount(@RequestBody @Valid AccountBean accountBean) {
        boolean result = accountService.insert(accountBean);
        return result; 
    }
    
    /**
     * 前端需要使用表单请求提交
     * @param accountBean
     * @return
     */
    @ApiOperation(value="用户增加", notes="根据用户信息进行保存")
    @PostMapping(path = "/insertAccountByForm")
    public boolean insertAccountByForm(@Valid AccountBean accountBean) {
        boolean result = accountService.insert(accountBean);
        return result; 
    }

}
