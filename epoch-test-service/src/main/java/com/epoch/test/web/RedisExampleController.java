 package com.epoch.test.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epoch.test.example.redis.RedisStringUtil;

import io.swagger.annotations.Api;

/**
 * @author zhangqinglei
 * @date 2020/12/15
 * @qq 544188838
 */
 @CrossOrigin
 @Api(value="Redis相关操作controller",tags={"Redis操作接口"})
 @RestController
 @RequestMapping("/epoch/study/redis")
public class RedisExampleController {
     
     @Autowired
     RedisStringUtil redisUtil;

     @PostMapping(path = "/setData")
     public boolean setData(String key, String value) {
         redisUtil.set(key, value);
         return true;
     }
}
