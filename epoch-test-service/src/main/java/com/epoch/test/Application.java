package com.epoch.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import springfox.documentation.oas.annotations.EnableOpenApi;
/**
 * 
 * @author zhangqinglei
 * @date 2020/12/02
 * @qq 544188838
 */
@SpringBootApplication(scanBasePackages="com.epoch")
@EnableConfigurationProperties
@EnableDiscoveryClient
@EnableOpenApi
public class Application {

    
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
