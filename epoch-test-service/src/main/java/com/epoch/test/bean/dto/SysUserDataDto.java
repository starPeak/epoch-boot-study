 package com.epoch.test.bean.dto;

 /**
 * @author zhangqinglei
 * @date 2020/12/19
 * @qq 544188838
 */
public class SysUserDataDto {

    private String account;
    
    private String token;

    /**
     * @param account
     * @param token
     */
    public SysUserDataDto(String account, String token) {
        super();
        this.account = account;
        this.token = token;
    }

    /**
     * @return the account
     */
    public String getAccount() {
        return account;
    }

    /**
     * @param account the account to set
     */
    public void setAccount(String account) {
        this.account = account;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }
    
    
}
