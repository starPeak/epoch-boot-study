package com.epoch.test.bean.activerecord.base;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.IBean;

/**
 * Jfinal中的Account类
 */
@SuppressWarnings("serial")
public abstract class BaseAccount<M extends BaseAccount<M>> extends Model<M> implements IBean {

	public void setID(java.lang.Integer ID) {
		set("ID", ID);
	}
	
	public java.lang.Integer getID() {
		return getInt("ID");
	}
	
	/**
	 * 账号
	 */
	public void setACCOUNT(java.lang.String ACCOUNT) {
		set("ACCOUNT", ACCOUNT);
	}
	
	/**
	 * 账号
	 */
	public java.lang.String getACCOUNT() {
		return getStr("ACCOUNT");
	}
	
	/**
	 * 密码
	 */
	public void setPASSWORD(java.lang.String PASSWORD) {
		set("PASSWORD", PASSWORD);
	}
	
	/**
	 * 密码
	 */
	public java.lang.String getPASSWORD() {
		return getStr("PASSWORD");
	}
	
	/**
	 * 真实姓名
	 */
	public void setRealName(java.lang.String realName) {
		set("REAL_NAME", realName);
	}
	
	/**
	 * 真实姓名
	 */
	public java.lang.String getRealName() {
		return getStr("REAL_NAME");
	}
	
	/**
	 * 邮箱
	 */
	public void setEMAIL(java.lang.String EMAIL) {
		set("EMAIL", EMAIL);
	}
	
	/**
	 * 邮箱
	 */
	public java.lang.String getEMAIL() {
		return getStr("EMAIL");
	}
	
	/**
	 * 手机
	 */
	public void setPHONE(java.lang.String PHONE) {
		set("PHONE", PHONE);
	}
	
	/**
	 * 手机
	 */
	public java.lang.String getPHONE() {
		return getStr("PHONE");
	}
	
	/**
	 * 身份证
	 */
	public void setIdCard(java.lang.String idCard) {
		set("ID_CARD", idCard);
	}
	
	/**
	 * 身份证
	 */
	public java.lang.String getIdCard() {
		return getStr("ID_CARD");
	}
	
	/**
	 * 生日
	 */
	public void setBIRTHDAY(java.util.Date BIRTHDAY) {
		set("BIRTHDAY", BIRTHDAY);
	}
	
	/**
	 * 生日
	 */
	public java.util.Date getBIRTHDAY() {
		return get("BIRTHDAY");
	}
	
	/**
	 * 状态，1启用，0禁用
	 */
	public void setSTATUS(java.lang.Integer STATUS) {
		set("STATUS", STATUS);
	}
	
	/**
	 * 状态，1启用，0禁用
	 */
	public java.lang.Integer getSTATUS() {
		return getInt("STATUS");
	}
	
	/**
	 * 创建日期
	 */
	public void setCreateDate(java.util.Date createDate) {
		set("CREATE_DATE", createDate);
	}
	
	/**
	 * 创建日期
	 */
	public java.util.Date getCreateDate() {
		return get("CREATE_DATE");
	}
	
}
