package com.epoch.test.bean.dto;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author zhangqinglei
 * @date 2020/12/17
 * @qq 544188838
 */
public class SysLoginDto implements Serializable {

    /**
    *
    */
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账号", example = "admin", notes = "登录账号")
    private String account;

    @ApiModelProperty(value = "密码", example = "123456", notes = "登录密码")
    private String password;

    @ApiModelProperty(value = "验证码", example = "1234", notes = "登录验证码")
    private String captcha;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
