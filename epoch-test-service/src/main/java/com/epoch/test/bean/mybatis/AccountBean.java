package com.epoch.test.bean.mybatis;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * 账号信息数据库对象
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
@ApiModel(value="账号对象",description="账号对象")
public class AccountBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @ApiModelProperty(required=false)
    private Integer id;

    @ApiModelProperty(value="用户名",example="epoch",required=true)
    @NotBlank(message="角色名称不能为空")
    private String account;

    @ApiModelProperty(value="密码",example="123456",required=true)
    @NotBlank(message="密码不能为空")
    private String password;

    @ApiModelProperty(value="真实姓名",example="汪峰",required=true)
    private String realName;

    @ApiModelProperty(value="邮箱",example="544188838@qq.com",required=false)
    private String email;

    @ApiModelProperty(value="电话",example="123456",required=false)
    private String phone;

    private String idCard;

    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date birthday;

    @ApiModelProperty(value="状态",required=false)
    private Integer status = 1;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}