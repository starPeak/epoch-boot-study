package com.epoch.test.bean.mybatis.query;

import io.swagger.annotations.ApiModelProperty;

public class BasePageQueury {

    @ApiModelProperty(value="页码",example="1",required=true)
    private Integer page =1;
    
    @ApiModelProperty(value="每页数目",example="10",required=true)
    private Integer pageSize = 10;

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
