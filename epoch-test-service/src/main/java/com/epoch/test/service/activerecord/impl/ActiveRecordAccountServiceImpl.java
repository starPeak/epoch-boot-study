package com.epoch.test.service.activerecord.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.epoch.test.bean.activerecord.Account;
import com.epoch.test.service.activerecord.IActiveRecordAccountService;
import com.jfinal.plugin.activerecord.Db;

@Service
public class ActiveRecordAccountServiceImpl implements IActiveRecordAccountService {

    @Override
    public boolean deleteByPrimaryKey(Integer id) {
        return true;
    }

    @Override
    public boolean insert(Account record) {
        return record.save();
    }

    @Override
    public Account selectByPrimaryKey(Integer id) {
        return Account.dao.findById(id);
    }

    @Override
    public boolean updateByPrimaryKeySelective(Account record) {
        return record.update();
    }

    @Override
    public boolean updateByPrimaryKey(Account record) {
        return record.update();
    }

    @Override
    public List<Account> selectAll() {
        return Account.dao.findAll();
    }

    @Override
    public boolean insertBatchAccount(List<Account> accountList) {
        Db.batchSave(accountList, accountList.size());
        return true;
    }

}
