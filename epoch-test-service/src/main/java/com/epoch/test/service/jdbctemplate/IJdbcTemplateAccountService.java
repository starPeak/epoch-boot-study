package com.epoch.test.service.jdbctemplate;

import java.util.List;

import com.epoch.test.bean.mybatis.AccountBean;

public interface IJdbcTemplateAccountService {

    boolean deleteByPrimaryKey(Integer id);

    boolean insert(AccountBean record);

    AccountBean selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKey(AccountBean record);
    
    List<AccountBean> selectAll();
}
