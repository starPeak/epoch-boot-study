package com.epoch.test.service.activerecord;

import java.util.List;

import com.epoch.test.bean.activerecord.Account;

/**
 * Service业务层
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
public interface IActiveRecordAccountService {
    
    boolean deleteByPrimaryKey(Integer id);

    boolean insert(Account record);

    Account selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKeySelective(Account record);

    boolean updateByPrimaryKey(Account record);
    
    List<Account> selectAll();

    boolean insertBatchAccount(List<Account> accountList);

}
