package com.epoch.test.service.jdbctemplate.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.RowMapperResultSetExtractor;
import org.springframework.stereotype.Service;

import com.epoch.test.bean.mybatis.AccountBean;
import com.epoch.test.service.jdbctemplate.IJdbcTemplateAccountService;

@Service
public class JdbcTemplateAccountServiceImpl implements IJdbcTemplateAccountService{
    
    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public boolean deleteByPrimaryKey(Integer id) {
        jdbcTemplate.update("delete from account where id = ?", id);
        return true;
    }

    @Override
    public boolean insert(AccountBean record) {
        jdbcTemplate.update("insert into account(ACCOUNT, PASSWORD, REAL_NAME, EMAIL, PHONE, ID_CARD, BIRTHDAY,STATUS, CREATE_DATE)"+
            " values(?,?,?,?,?,?,?,?,?)",new Object[] {record.getAccount(),record.getPassword(),record.getRealName(),
                record.getEmail(),record.getPhone(),record.getIdCard(),record.getBirthday(),record.getStatus(),record.getCreateDate()});
         return true;
    }

    @Override
    public AccountBean selectByPrimaryKey(Integer id) {
        AccountBean account = jdbcTemplate.queryForObject("seelct * from account where id = ?",new RowMapper<AccountBean>() {
            @Override
            public AccountBean mapRow(ResultSet rs, int rowNum) throws SQLException {
                AccountBean bean = new AccountBean();
                bean.setIdCard(rs.getString("ID_CARD"));
                bean.setAccount(rs.getString("ACCOUNT"));
                bean.setEmail(rs.getString("EMAIL"));
                bean.setRealName(rs.getString("REAL_NAME"));
                bean.setPassword(rs.getString("PASSWORD"));
                bean.setStatus(rs.getInt("PASSWORD"));
                bean.setBirthday(rs.getDate("BIRTHDAY"));
                bean.setCreateDate(rs.getDate("CREATE_DATE"));
                bean.setPhone(rs.getString("PHONE"));
                return bean;
            }
        },id);
        return account;
    }

    @Override
    public boolean updateByPrimaryKey(AccountBean record) {
        String sql = "update account set ACCOUNT = ?,PASSWORD = ?,REAL_NAME = ?,"
            + "EMAIL = ?,PHONE = ?,ID_CARD = ?,BIRTHDAY = ?,STATUS = ?,CREATE_DATE = ? where ID = ?";
        jdbcTemplate.update(sql,new Object[] {record.getAccount(),record.getPassword(),record.getRealName(),
            record.getEmail(),record.getPhone(),record.getIdCard(),record.getBirthday(),record.getStatus(),record.getCreateDate(),record.getId()});
        return true;
    }

    @Override
    public List<AccountBean> selectAll() {
        List<AccountBean> accountList = jdbcTemplate.query("seelct * from account",new RowMapper<AccountBean>() {
            @Override
            public AccountBean mapRow(ResultSet rs, int rowNum) throws SQLException {
                AccountBean bean = new AccountBean();
                bean.setIdCard(rs.getString("ID_CARD"));
                bean.setAccount(rs.getString("ACCOUNT"));
                bean.setEmail(rs.getString("EMAIL"));
                bean.setRealName(rs.getString("REAL_NAME"));
                bean.setPassword(rs.getString("PASSWORD"));
                bean.setStatus(rs.getInt("PASSWORD"));
                bean.setBirthday(rs.getDate("BIRTHDAY"));
                bean.setCreateDate(rs.getDate("CREATE_DATE"));
                bean.setPhone(rs.getString("PHONE"));
                return bean;
            }
        });
        return accountList;
    }

}
