package com.epoch.test.service.mybatis.impl;

import java.util.List;

import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epoch.test.bean.mybatis.AccountBean;
import com.epoch.test.dao.mybatis.AccountMapper;
import com.epoch.test.service.mybatis.IMybatisAccountService;

@Service
public class MybatisAccountServiceImpl implements IMybatisAccountService {

    private static Logger logger = LoggerFactory.getLogger(MybatisAccountServiceImpl.class);

    @Autowired
    AccountMapper accountMapper;

    @Autowired
    @Qualifier("mybatisSqlSessionFactory")
    SqlSessionFactory sqlSessionFactory;

    /**
     * 业务复杂的时候，需要增加事务，事务的传播只在当前线程内传播，无法跨线程
     */
    @Transactional(value = "mybatisTransactionManager", rollbackFor = Exception.class)
    @Override
    public boolean deleteByPrimaryKey(Integer id) {
        return accountMapper.deleteByPrimaryKey(id);
    }

    @Transactional(value = "mybatisTransactionManager", rollbackFor = Exception.class)
    @Override
    public boolean insert(AccountBean record) {
        return accountMapper.insert(record);
    }

    /**
     * 查询的时候，不需要加事务注解
     */
    @Override
    public AccountBean selectByPrimaryKey(Integer id) {
        return accountMapper.selectByPrimaryKey(id);
    }

    @Transactional(value = "mybatisTransactionManager", rollbackFor = Exception.class)
    @Override
    public boolean updateByPrimaryKeySelective(AccountBean record) {
        return accountMapper.updateByPrimaryKeySelective(record);
    }

    @Transactional(value = "mybatisTransactionManager", rollbackFor = Exception.class)
    @Override
    public boolean updateByPrimaryKey(AccountBean record) {
        return accountMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<AccountBean> selectAll() {
        return accountMapper.selectAll();
    }

    @Override
    public boolean insertBatchAccount1(List<AccountBean> accountList) {
        return accountMapper.insertBatchAccount1(accountList);
    }

    @Override
    public boolean insertBatchAccount2(List<AccountBean> accountList) {
        SqlSession sqlSession = sqlSessionFactory.openSession(ExecutorType.BATCH);
        try {
            AccountMapper accountMapper = sqlSession.getMapper(AccountMapper.class);
            for (AccountBean accountBean : accountList) {
                accountMapper.insert(accountBean);
            }
            sqlSession.commit();
        } catch (Exception e) {
            logger.error("insertBatchAccount2 is error", e);
        } finally {
            sqlSession.close();
        }
        return true;
    }

}
