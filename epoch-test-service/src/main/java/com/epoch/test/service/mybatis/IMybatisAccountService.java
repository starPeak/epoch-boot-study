package com.epoch.test.service.mybatis;

import java.util.List;

import com.epoch.test.bean.mybatis.AccountBean;

/**
 * Service业务层
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
public interface IMybatisAccountService {
    
    boolean deleteByPrimaryKey(Integer id);

    boolean insert(AccountBean record);

    AccountBean selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKeySelective(AccountBean record);

    boolean updateByPrimaryKey(AccountBean record);
    
    List<AccountBean> selectAll();
    
    /**
     * 等价于
     * insert into account (ACCOUNT, PASSWORD,REAL_NAME, EMAIL, PHONE,ID_CARD, BIRTHDAY, STATUS,CREATE_DATE)
     * values
     * (?,?,?,?,?,?,?,?,?),(?,?,?,?,?,?,?,?,?),(?,?,?,?,?,?,?,?,?),(?,?,?,?,?,?,?,?,?)
     * 
     * Oracle中限定符不能超过65535，注意，且限定符?越多，速率越慢，请在Oracle的代码工程查看使用方案
     * @param accountList
     * @return
     */
    boolean insertBatchAccount1(List<AccountBean> accountList);
    
    /**
     * 使用batch模式，建议大数据量的时候提交
     * @param accountList
     * @return
     */
    boolean insertBatchAccount2(List<AccountBean> accountList);

}
