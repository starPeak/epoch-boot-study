package com.epoch.test.service.jpa.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epoch.test.bean.jpa.Account;
import com.epoch.test.dao.jpa.AccountRepository;
import com.epoch.test.service.jpa.IJpaAccountService;

@Service
public class JpaAccountServiceImpl implements IJpaAccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public boolean deleteByPrimaryKey(Integer id) {
        accountRepository.deleteById(id);
        return true;
    }

    @Override
    @Transactional(value = "jpaTransactionManager")
    public boolean insert(Account record) {
        accountRepository.save(record);
        return true;
    }

    @Override
    public Account selectByPrimaryKey(Integer id) {
        Optional<Account> account = accountRepository.findById(id);
        return account.get();
    }

    @Override
    public boolean updateByPrimaryKeySelective(Account record) {
        accountRepository.saveAndFlush(record);
        return true;
    }

    @Override
    public boolean updateByPrimaryKey(Account record) {
        accountRepository.saveAndFlush(record);
        return true;
    }

    @Override
    public List<Account> selectAll() {
        return accountRepository.findAll();
    }

    @Override
    public boolean insertBatchAccount(List<Account> accountList) {
        accountRepository.saveAll(accountList);
        return true;
    }

}
