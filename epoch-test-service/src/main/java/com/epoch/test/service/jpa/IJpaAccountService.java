package com.epoch.test.service.jpa;

import java.util.List;

import com.epoch.test.bean.jpa.Account;

public interface IJpaAccountService {

    boolean deleteByPrimaryKey(Integer id);

    boolean insert(Account record);

    Account selectByPrimaryKey(Integer id);

    boolean updateByPrimaryKeySelective(Account record);

    boolean updateByPrimaryKey(Account record);
    
    List<Account> selectAll();

    boolean insertBatchAccount(List<Account> accountList);

}
