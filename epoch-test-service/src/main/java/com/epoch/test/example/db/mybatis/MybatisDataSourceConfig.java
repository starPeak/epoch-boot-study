package com.epoch.test.example.db.mybatis;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import com.alibaba.druid.pool.DruidDataSource;
import com.github.pagehelper.PageInterceptor;

@Configuration
@MapperScan(basePackages = MybatisDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "mybatisSqlSessionFactory")
public class MybatisDataSourceConfig {

    protected static final String PACKAGE = "com.epoch.test.dao.mybatis";
    protected static final String MAPPER_MYSQL_LOCATION = "classpath:mapper/mysql/*.xml";
    protected static final String MAPPER_ORACLE_LOCATION = "classpath:mapper/oracle/*.xml";

    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.datasource.test")
    public DataSource dataSource() {
        return new DruidDataSource();
    }

    @Bean(name = "mybatisTransactionManager")
    public DataSourceTransactionManager mybatisTransactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean(name = "mybatisSqlSessionFactory")
    public SqlSessionFactory clusterSqlSessionFactory(DataSource dataSource)
        throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setMapperLocations(
            new PathMatchingResourcePatternResolver().getResources(MybatisDataSourceConfig.MAPPER_MYSQL_LOCATION));
        //分页插件
        sessionFactory.setPlugins(new Interceptor[] { getMysqlInterceptor() });
        return sessionFactory.getObject();
    }
    
    /**
     * 获取Mysql分页
     * @return
     */
    private Interceptor getMysqlInterceptor() {
        Interceptor interceptor = new PageInterceptor();
        Properties properties = new Properties();
        //数据库
        properties.setProperty("helperDialect", "mysql");
        //是否将参数offset作为PageNum使用
        properties.setProperty("offsetAsPageNum", "true");
        //是否进行count查询
        properties.setProperty("rowBoundsWithCount", "true");
        //是否分页合理化
        properties.setProperty("reasonable", "false");
        interceptor.setProperties(properties);
        return interceptor;
    }
}
