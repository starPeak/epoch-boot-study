package com.epoch.test.example.db.activerecord;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.wall.WallFilter;
import com.epoch.test.bean.activerecord._MappingKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.source.ClassPathSourceFactory;
/**
 * Jfinal的activeRecord连接
 * 这个数据库层也有一些好用的地方，可以参考网站
 * https://gitee.com/jfinal/activerecord
 * 用法参考
 * 1.http://www.jfinal.com/doc/5-1   第五章节
 * 2.本文案例
 * @author zhangqinglei
 * @date 2020/12/14
 * @qq 544188838
 */
@Configuration
public class SpringActiveRecordConfig {

    @Value("${spring.datasource.dbType}")
    private String dbType;
    
    @Value("${spring.datasource.test.url}")
    private String url;

    @Value("${spring.datasource.test.username}")
    private String username;
    
    @Value("${spring.datasource.test.password}")
    private String password;

    public ActiveRecordPlugin initActiveRecordPlugin() {

        DruidPlugin druidPlugin = new DruidPlugin(url, username, password);
        // 加强数据库安全
        WallFilter wallFilter = new WallFilter();
        wallFilter.setDbType("mysql");
        druidPlugin.addFilter(wallFilter);
        // 添加 StatFilter 才会有统计数据
        // druidPlugin.addFilter(new StatFilter());
        // 必须手动调用start
        druidPlugin.start();

        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setTransactionLevel(Connection.TRANSACTION_READ_COMMITTED);
        _MappingKit.mapping(arp);
        arp.setShowSql(false);

        arp.getEngine().setSourceFactory(new ClassPathSourceFactory());
        arp.addSqlTemplate("sql/all_sqls.sql");
        // 必须手动调用start
        arp.start();
        return arp;
    }

}
