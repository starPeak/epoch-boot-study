package com.epoch.test.example.cron;

import java.time.LocalDateTime;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * schedule定时任务
 * @author zhangqinglei
 * @date 2020/12/02
 */
@Configuration
@EnableScheduling
public class ScheduleTaskDemo {

    /**
     * 缺点：
     * 1.分布式多服务的时候，每个服务都会执行，可能会出现业务问题
     * 2.动态调整的不方便，需要重启应用才能生效。
     * 或直接指定时间间隔，例如：5秒
     * @Scheduled(fixedRate=5000)
     */
    //@Scheduled(cron = "0/30 * * * * ?")
    @SuppressWarnings("unused")
    private void configureTasks() {
        System.out.println("执行静态定时任务时间: " + LocalDateTime.now());
    }
}
