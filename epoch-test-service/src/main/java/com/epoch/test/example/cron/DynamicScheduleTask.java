package com.epoch.test.example.cron;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.scheduling.support.CronTrigger;
/**
 * 
 * 虽然在数据库配置了，可以自由配置，但是任然不支持分布式
 * @author zhangqinglei
 * @date 2020/12/02
 */
@Configuration
public class DynamicScheduleTask implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(new TaskDemo(), triggerContext -> {
            // 2.1 从数据库获取执行周期
            String cron = "0 0 2 * * ?";
            // 2.2 合法性校验.
            if (StringUtils.isEmpty(cron)) {
                // Omitted Code ..
            }
            // 2.3 返回执行周期 (Date)
            return new CronTrigger(cron).nextExecutionTime(triggerContext);
        });
    }

    static class TaskDemo implements Runnable {

        @Override
        public void run() {
            System.out.println("定时任务运行");
        }

    }
}
