# 微服务以及大量案例代码

#### 介绍

EpochTest是一个集成SpringBoot的案例代码，前后端分离,后端主要有，Mybatis,JdbcTemplate,Jdbc,JPA,多线程,Cron,文件上传下载,POI导出,CSV导出等大量的案例使用,可以直接应用。前端使用React，Vue，ExtJs等等的各种大量的案例开发(解决跨域问题)。

关于分支和项目的说明

1.master主要是mysql集成的案例，后续会增加针对oracle以及其他 **国产数据库(比如达梦)**等支持，目前是针对查询，插入(批量插入)，更新，删除，分页展示等大量案例的集成和应用，使用了不同的数据库层框架(主要是Mybatis,JPA,ActiveRecord,SpringTemplate,JDBC演示)。 

2.前端后续会增加案例演示，主要是Extjs6,React,Vue等版本，主要为前后端分离演示，前端在单独的项目中， **目前先完成后端模块，后续增加前端(一个人忙不过来)** 。

重点

前端Extjs版本：https://gitee.com/zzhangqinglei/epoch-test-extjs

前端React版本：https://gitee.com/zzhangqinglei/epoch-test-react

因美国制裁，后续数据库，JDK版本优先支持国产开源，还有服务器部署。

#### 工程包功能划分如下

| 工程                 | 说明                       | 路径   | 端口号|
|--------------------|--------------------------|------|------|
| epoch-eureka       | 服务注册中心，使用了eureka进行服务注册管理 | 当前项目 |5060 |
| epoch-common| 公共模块集成        | 当前项目 |无 |
| epoch-api| 公共模块集成，多服务之间的api调用注册        | 当前项目 |无 |
| epoch-test-service | 涵盖大量的案例，主要集成案例代码         | 当前项目 |7100 |
| epoch-web-service  | 前端项目部署的模块，打包到这个目录进行部署    | 当前项目 | 6868     |
| epoch-plan-service  | 用户登录，用户权限，角色权限，网关等等大量的功能，使用JWT进行认证，使用Shiro+Zuul进行权限网关认证   | 当前项目 | 6869     |
| epoch-oauth2  | 使用OAuth2实现多个微服务的统一认证授权，适配所有服务   | 当前项目 | 6870     |
| epoch-front-extjs  | 前端代码开发，extjs版本，基于extjs6.8版本进行开发，本地使用sencha开发，服务端打包到epoch-web-service部署    |  https://gitee.com/zzhangqinglei/epoch-test-extjs| 6868     |
| epoch-front-react  | 前端代码开发，react版本，基于react,ant等版本进行开发，本地使用node开发，服务端打包到epoch-web-service部署   | https://gitee.com/zzhangqinglei/epoch-test-react| 6868     |


#### 软件版本如下

| 软件          | 版本说明                             |
|-------------|----------------------------------|
| SpringBoot  | 2.4.1                            |
| SpringCloud | 2020.0.0-RC1                     |
| Springfox   | 基于最新的3.0.0版本开发，案例已集成完整，包含了JwtToken校验                     |
| Mybatis     | 数据库访问层，目前市面上常用，使用简单，集成了pagehelper的分页插件，涵盖大量的演示案例，我会重点演示大数据量性能相关代码(比如大量数据插入)(我目前工作使用Mybatis，读过源码，有培训) |
| activerecord            | Jfinal提供的数据库访问层，使用简单，具体参考文档 http://www.jfinal.com/doc/5-1 ，本文也给了大量的案例。我个人比较喜欢，有优点，操作数据库简单，多数据源切换方面，缺点目前可能大型框架我不是很熟悉，中小型应用推荐一下。                        |
| JPA         | 目前市面上比较通用的ORM框架，本文给出了大量的案例代码开发。                             |
| Mysql       | 8.0版本，后续支持5.7版本。(目前也应该支持)                        |
| Oracle      | 10g以及以上                             |
| Ftp操作     | FTP使用了common-net，io,等相关包，SFTP使用了jsch包                                |
| 服务注册中心     | zookeeper                               |
| RPC     | weipo-motan等框架使用集成                              |



主要包含如下(同步更新中)

### 1.SpringBoot自身

#### 1.1.SpringBoot->Swagger** 

参考类SwaggerConfig

这里说明一下springfox-swagger2使用的是2.8版本，请勿升级到2.9版本。

2.9版本中有crsf验证问题，具体参考网站：https://github.com/springfox/springfox/issues/2603

后续会升级到2.9版本。

#### 1.2.SpringBoot定时任务的俩种方案集合** 

参考类：DynamicScheduleTask，ScheduleTaskDemo

#### 1.3.Spring线程池案例整合** 

### 2.文件操作

#### 2.1.SpringBoot->文件上传下载案例** 
#### 2.2.Ftp,Sftp文件上传下载等等案例**
#### 2.3.POI文件导出，支持XLS和XLSX格式**
#### 2.4.txt文件导出**

### 3.数据库方面

#### 3.1 Mybatis大量案例开发

整合参考类：MybatisDataSourceConfig

案例参考类：

Dao层：com.epoch.dao.mybatis.AccountMapper

Service层：com.epoch.service.mybatis.IMybatisAccountService

controller层：com.epoch.web.mybatis包下类

#### 3.2 JPA大量案例开发
#### 3.3 ActiveRecord大量案例开发
#### 3.4 JdbcTemplate大量案例开发
#### 3.5 Jdbc大量案例开发

### 4.XML

#### 4.1 SAX解析XML案例

### 5.Redis

#### 5.1 常用Redis案例
#### 5.2 分布式锁案例
#### 5.3 分布式队列案例

### 6.消息中间件

#### 6.1 RabbitMq
#### 6.2 Kafka

### 7.常用工具类

#### 7.1 时间公共类
#### 7.2 POI公共样式处理类
#### 7.3 Ftp工具类

位于com.epoch.example.ftp包下，核心接口类IFtpClient

工厂类获取：FtpUtils，模块使用注入，调用getFtpClient方法

实现：支持FTP模式和SFTP模式，FTP使用apache-common相关包实现，SFTP使用jsch包实现。

配置参数，进行如下YML配置：


```
ftp:
  ip: 123456
  username: root
  password: 123456
  port: 21

```

### 8.Shiro-Jwt鉴权校验案例

#### 8.1 使用Shiro-Jwt鉴权机制

使用Shiro-Jwt鉴权机制，支持单点登录等。


#### 参与贡献

如果有需要一起参与的，不管是前端还是后端，欢迎到来。

请联系我QQ544188838

1.共同增加更多的案例开发

2.如有需求，可以沟通，后续补充更新。


