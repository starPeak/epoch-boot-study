package com.epoch.common.ftp;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;
/**
 * FTP配置，读取ftp下的文件
 * @author zhangqinglei
 * @date 2020/12/15
 * @qq 544188838
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix="ftp")
public class FtpInfo {

    private String ip;
    
    private String username;
    
    private String password;
    
    private int port = 21;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
