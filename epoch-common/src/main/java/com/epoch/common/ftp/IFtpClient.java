package com.epoch.common.ftp;

/**
 * FTP接口，实现主要有FTP，SFTP俩种模式
 * @author zhangqinglei
 * @date 2020/12/11
 * @qq 544188838
 */
public interface IFtpClient {

    /**
     * 将本地文件，上传到远程目录，
     * 远程目录不要用以/开头，上传到配置用户的家目录
     * @param local
     * @param remote
     * @return
     */
    public boolean uploadFile(String local,String remote);
    
    /**
     * 删除远程服务器的文件
     * @param remoteFile
     * @return
     */
    public boolean deleteRemoteFile(String remoteFile);
    
    /**
     * 下载文件
     * @param local
     * @param remote
     * @return
     */
    public boolean downloadFile(String local, String remote);
    
    /**
     * 释放连接，如果某些请求不释放，就主动调用一下
     * @return
     */
    public boolean closeFtpClient();
    
    /**
     * 更改工作目录，此操作不会关闭客户端
     * @param changeDir
     * @return
     */
    public boolean changeDir(String changeDir);
}
