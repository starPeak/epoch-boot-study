package com.epoch.common.ftp.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;

import com.epoch.common.ftp.FtpInfo;
import com.epoch.common.ftp.IFtpClient;

/**
 * FTP的实现类，主要是由apache common实现
 * FTP模式，端口号默认是21
 * @author zhangqinglei
 * @date 2020/12/11
 * @qq 544188838
 */
public class FtpLocalClient implements IFtpClient {

	private FtpInfo ftpInfo;

	private FTPClient ftpClient;

	public FtpLocalClient(FtpInfo ftpInfo) {
		super();
		this.ftpInfo = ftpInfo;
	}

	public boolean connectServer() {
		ftpClient = new FTPClient();
		try {
			ftpClient.connect(ftpInfo.getIp(), ftpInfo.getPort());
			if (FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
				if (ftpClient.login(ftpInfo.getUsername(), ftpInfo.getPassword())) {
					return true;
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean uploadFile(String local, String remote) {
		boolean connect = connectServer();
		try (BufferedInputStream inStream = new BufferedInputStream(new FileInputStream(local))) {
			if (connect) {
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileTransferMode(FTP.STREAM_TRANSFER_MODE);
				ftpClient.storeFile(remote, inStream);
				return true;
			}
		} catch (Exception e) {

		} finally {
			closeFtpClient();
		}
		return false;
	}

	@Override
	public boolean deleteRemoteFile(String remoteFile) {
		
		return false;
	}

	@Override
	public boolean downloadFile(String local, String remote) {
		boolean connect = connectServer();
		BufferedOutputStream outStream = null;
		boolean rs = false;
		try {
			if (connect) {
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
				outStream = new BufferedOutputStream(new FileOutputStream(local));
				rs = ftpClient.retrieveFile(remote, outStream);
				return rs;
			}
		} catch (Exception e) {

		} finally {
			if (outStream != null) {
				try {
					outStream.flush();
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			closeFtpClient();
		}
		return false;
	}

	@Override
	public boolean closeFtpClient() {
		try {
			if (null != ftpClient && ftpClient.isConnected()) {
				ftpClient.disconnect();
				ftpClient.logout();
			}
			return true;
		} catch (IOException e) {
			
		}
		return false;
	}

	@Override
	public boolean changeDir(String changeDir) {
		try {
			ftpClient.changeWorkingDirectory(changeDir);
			return true;
		} catch (IOException e) {
			
		}  
		return false;
	}

}
