package com.epoch.common.ftp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.epoch.common.ftp.impl.FtpLocalClient;
import com.epoch.common.ftp.impl.SFtpClient;

@Component
public class FtpUtils {
    
    @Autowired
    FtpInfo ftpInfo;

    /**
     * 获取FTP的实现类
     * @return
     */
    public IFtpClient getFtpClient() {
        IFtpClient client = null;
        if(ftpInfo.getPort() == 21) {
            client = new FtpLocalClient(ftpInfo);
        }else {
            client = new SFtpClient(ftpInfo);
        }
        return client;
    }
}
