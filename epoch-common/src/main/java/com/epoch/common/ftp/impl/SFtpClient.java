package com.epoch.common.ftp.impl;

import com.epoch.common.ftp.FtpInfo;
import com.epoch.common.ftp.IFtpClient;

/**
 * SFTP的实现，主要是由JSCH原理实现
 * 
 * @author zhangqinglei
 * @date 2020/12/11
 * @qq 544188838
 */
public class SFtpClient implements IFtpClient {

    private FtpInfo ftpInfo;

    public SFtpClient(FtpInfo ftpInfo) {
        super();
        this.ftpInfo = ftpInfo;
    }

    @Override
    public boolean uploadFile(String local, String remote) {
        return false;
    }

    @Override
    public boolean deleteRemoteFile(String remoteFile) {
         return false;
    }

	@Override
	public boolean downloadFile(String local, String remote) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean closeFtpClient() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean changeDir(String changeDir) {
		// TODO Auto-generated method stub
		return false;
	}
}
