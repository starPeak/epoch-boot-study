package com.epoch.common.exception;

/**
 * 自定义401无权限异常(UnauthorizedException)
 */
public class BussinessUnauthorizedException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public BussinessUnauthorizedException(String msg) {
        super(msg);
    }

    public BussinessUnauthorizedException() {
        super();
    }
}
