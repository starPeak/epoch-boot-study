package com.epoch.common.exception;

/**
 * 业务类基础异常
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
public class BussinessException extends RuntimeException{

   /**
     *
     */
   private static final long serialVersionUID = 1L;

   public BussinessException(String msg){
       super(msg);
   }

   public BussinessException() {
       super();
   }
}