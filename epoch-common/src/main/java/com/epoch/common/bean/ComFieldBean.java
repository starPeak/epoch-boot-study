package com.epoch.common.bean;
/**
 * 适配前端下拉框等，需要用这个对象构造List进行返回
 * @author zhangqinglei
 * @date 2021/01/11
 * @qq 544188838
 */
public class ComFieldBean {

	/**
	 * 显示到界面的参数名称
	 */
	private String text;
	
	/**
	 * 真实值
	 */
	private Object value;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}
