package com.epoch.common.bean;

import java.util.List;

public class EpochTree {

	private String text = "空父节点";
	
	private boolean leaf = false;
	
	private boolean expanded  = true;
	
	private List<? extends BaseTreeNode> children;
	
	public EpochTree() {
		super();
	}

	public EpochTree(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isLeaf() {
		return leaf;
	}

	public void setLeaf(boolean leaf) {
		this.leaf = leaf;
	}

	public boolean isExpanded() {
		return expanded;
	}

	public void setExpanded(boolean expanded) {
		this.expanded = expanded;
	}

	public List<? extends BaseTreeNode> getChildren() {
		return children;
	}

	public void setChildren(List<? extends BaseTreeNode> children) {
		this.children = children;
	}
	
}
