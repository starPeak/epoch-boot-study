 package com.epoch.common.response;

import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import com.epoch.common.bean.PageResponseBean;
import com.epoch.common.bean.ResponseBean;

/**
 * 公共拦截
 * @author zhangqinglei
 * @date 2020/12/09
 * @qq 544188838
 */
@RestControllerAdvice
public class GlobalResponseHandler implements ResponseBodyAdvice<Object>{

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        if(returnType.getGenericParameterType().equals(PageResponseBean.class)) {
            return false;
        }
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
        Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
        ServerHttpResponse response) {
        ServletServerHttpRequest serverRequest = (ServletServerHttpRequest)request;
        ServletServerHttpResponse serverResponse = (ServletServerHttpResponse)response;
        if(serverRequest == null || serverResponse == null
                || serverRequest.getServletRequest() == null || serverResponse.getServletResponse() == null) {
            return body;
        }
        /**
         * 处理不拦截请求
         */
        if(!returnType.getExecutable().getDeclaringClass().getName().startsWith("com.epoch")) {
            return body;
        }
        ResponseBean reponseBean = null;
        if(body instanceof ResponseBean) {
        	reponseBean = (ResponseBean) body;
        	if(reponseBean.getCode() == 200) {
        		reponseBean.setSuccess(true);
        	}
            return reponseBean;
        }
        reponseBean = new  ResponseBean();
        reponseBean.setData(body);
        reponseBean.setCode(200);
        reponseBean.setMsg("操作成功");
        return reponseBean;
    }

}
